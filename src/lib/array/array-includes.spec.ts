import { arrayIncludes } from './array-includes';

const array: any[] = [1, 'a', true, { a: 1 }, null, undefined];

describe('arrayIncludes(array, searchElement, fromIndex?)', () => {
  test('returns true if exist number', () => {
    expect(arrayIncludes(array, 1)).toEqual(true);
  });

  test('returns true if exist string', () => {
    expect(arrayIncludes(array, 'a')).toEqual(true);
  });

  test('returns true if exist boolean', () => {
    expect(arrayIncludes(array, true)).toEqual(true);
  });

  test('returns true if exist null', () => {
    expect(arrayIncludes(array, null)).toEqual(true);
  });

  test('returns true if exist undefined', () => {
    expect(arrayIncludes(array, undefined)).toEqual(true);
  });

  test('returns false if exist objects', () => {
    expect(arrayIncludes(array, { a: 1 })).toEqual(false);
  });

  test('returns false if element does not exist', () => {
    expect(arrayIncludes(array, 0)).toEqual(false);
  });

  test('use fromIndex with positive value from start', () => {
    expect(arrayIncludes(array, 1, 1)).toEqual(false);
    expect(arrayIncludes(array, 'a', 1)).toEqual(true);
  });

  test('use fromIndex with negative value from end', () => {
    expect(arrayIncludes(array, null, -1)).toEqual(false);
    expect(arrayIncludes(array, undefined, -1)).toEqual(true);
  });
});
