/**
 * Check that the value is a non empty Array.
 *
 * @param value The value to test.
 * @returns True if the value is an non empty Array.
 */
export function isNonEmptyArray<T>(value: any): value is T[] {
  return typeof value === 'object' && value instanceof Array && value.length > 0;
}
