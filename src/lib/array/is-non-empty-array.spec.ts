import { isNonEmptyArray } from './is-non-empty-array';

describe('isNonEmptyArray(value)', () => {
  test('returns true as type guard if value is not an empty Array', () => {
    expect(isNonEmptyArray([1])).toBeTruthy();
  });

  test('returns false as type guard if value is an empty Array', () => {
    expect(isNonEmptyArray([])).toBeFalsy();
  });
});
