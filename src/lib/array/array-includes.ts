import { sameValueZero } from '../same-value-zero';

/**
 * Determines whether an array includes a certain element.
 *
 * @param array The array to search from.
 * @param searchElement The element to search for.
 * @param [fromIndex] The position in this array at which to begin searching for searchElement.
 * A negative value searches from the index of array.length - fromIndex by asc. Defaults to 0.
 * @returns `true` if the value searchElement is found within the array, if not `false`.
 */
export function arrayIncludes(array: any[], searchElement: any, fromIndex?: number): boolean {
  const length: number = array.length;

  if (length === 0) {
    return false;
  }

  const from: number = fromIndex !== undefined ? fromIndex : 0;
  let pos: number = Math.max(from >= 0 ? from : length - Math.abs(from), 0);

  while (pos < length) {
    if (sameValueZero(array[pos], searchElement)) {
      return true;
    }
    pos += 1;
  }

  return false;
}
