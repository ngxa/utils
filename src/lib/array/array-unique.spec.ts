import { arrayUnique } from './array-unique';

const array: any[] = [1, 2, 3];

describe('arrayUnique(values)', () => {
  test('returns the same array if it does not have duplicates', () => {
    expect(arrayUnique(array)).toEqual(array);
  });

  test('returns an array with deleted duplicates', () => {
    expect(arrayUnique([...array, ...array, array[0]])).toEqual(array);
  });
});
