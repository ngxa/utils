/**
 * Delete the duplicate elements of an array.
 *
 * @param values The array from which the duplicates will be eliminated.
 * @returns the provided array with unique elements.
 */
export function arrayUnique(values: any[]): any[] {
  return Array.from(new Set(values));
}
