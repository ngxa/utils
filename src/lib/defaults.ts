import { isVoid } from './typings/is-void';

/**
 * Return the default value if the original is null or undefined.
 *
 * @param value The original value.
 * @param defaultValue The default value.
 * @returns The default value if the original is null or undefined.
 */
export function defaults<T = any>(value: any, defaultValue: T): T {
  return !isVoid(value) ? value : defaultValue;
}
