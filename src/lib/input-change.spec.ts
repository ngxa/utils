import { SimpleChange } from '@angular/core';

import { inputChange } from './input-change';

describe('inputChange(input, changes)', () => {
  it('returns true if the input has changed', () => {
    expect(inputChange('input', { input: new SimpleChange(undefined, true, true) })).toEqual(true);
  });

  it('returns false if the input has the same value', () => {
    expect(inputChange('input', { input: new SimpleChange(true, true, false) })).toEqual(false);
  });

  it('returns false if the input is undefined', () => {
    expect(inputChange('input', { input: new SimpleChange(undefined, undefined, false) })).toEqual(
      false
    );
  });

  it('returns false if the input do not exist in SimpleChange', () => {
    expect(inputChange('input', { input2: new SimpleChange(undefined, true, false) })).toEqual(
      false
    );
  });
});
