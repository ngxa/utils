/**
 * Determines whether two values are the same value. Differs from `Object.is` only in its
 * treatment of +0 and -0.
 *
 * @param value1 The first value to compare.
 * @param value2 The second value to compare.
 * @returns A Boolean indicating whether or not the two arguments are the same value.
 */
export function sameValueZero(value1: any, value2: any): boolean {
  return (
    value1 === value2 ||
    (typeof value1 === 'number' && typeof value2 === 'number' && isNaN(value1) && isNaN(value2))
  );
}
