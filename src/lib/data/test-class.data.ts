import { asyncScheduler, Observable, of } from 'rxjs';
import { ParentTestClass } from './parent-test-class.data';

/**
 * Test class
 */
export class TestClass extends ParentTestClass {
  public prop: string = 'c';
  public undefinedProp!: string;
  public nullProp: any = null;
  private _getterSetterProp?: string;

  public get getterSetterProp(): string | undefined {
    return this._getterSetterProp;
  }

  public set getterSetterProp(value: string | undefined) {
    this._getterSetterProp = value;
  }

  public get getterProp(): string | undefined {
    return this._getterSetterProp;
  }

  public set setterProp(value: string | undefined) {
    this._getterSetterProp = value;
  }

  public method(value?: any): any {
    return value;
  }

  public methodObservable(value?: any): Observable<any> {
    return of(value, asyncScheduler);
  }

  public methodPromise(value?: any): Promise<any> {
    return Promise.resolve(value);
  }

  public callMethod(value?: any): void {
    this.privateMethod(value);
  }

  public getNullProp(): any {
    return this.nullProp;
  }

  public setNullProp(value?: any): void {
    this.nullProp = value !== undefined ? value : 'c';
  }

  private privateMethod(value?: any): any {
    return this.method(value);
  }
}
