import { asyncScheduler, Observable, of } from 'rxjs';

/**
 * Other Test class
 */
export class OtherTestClass {
  public otherProp: string = 'o';
  public undefinedOtherProp!: string;
  public nullOtherProp: any = null;
  private _getterSetterOtherProp?: string;

  public get getterSetterOtherProp(): string | undefined {
    return this._getterSetterOtherProp;
  }

  public set getterSetterOtherProp(value: string | undefined) {
    this._getterSetterOtherProp = value;
  }

  public get getterOtherProp(): string | undefined {
    return this._getterSetterOtherProp;
  }

  public set setterOtherProp(value: string | undefined) {
    this._getterSetterOtherProp = value;
  }

  public otherMethod(value?: any): any {
    return value;
  }

  public otherMethodObservable(value?: any): Observable<any> {
    return of(value, asyncScheduler);
  }

  public otherMethodPromise(value?: any): Promise<any> {
    return Promise.resolve(value);
  }

  public otherCallMethod(value?: any): void {
    this.otherPrivateMethod(value);
  }

  public getNullOtherProp(): any {
    return this.nullOtherProp;
  }

  public setNullOtherProp(value?: any): void {
    this.nullOtherProp = value !== undefined ? value : 'o';
  }

  private otherPrivateMethod(value?: any): any {
    return this.otherMethod(value);
  }
}
