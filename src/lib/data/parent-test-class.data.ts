import { GrandTestClass } from './grand-test-class.data';

/**
 * Parent test class
 */
export class ParentTestClass extends GrandTestClass {
  public parentProp: string = 'p';
  public undefinedParentProp!: string;
  public nullParentProp: any = null;
  private _getterSetterParentProp?: string;

  public get getterSetterParentProp(): string | undefined {
    return this._getterSetterParentProp;
  }

  public set getterSetterParentProp(value: string | undefined) {
    this._getterSetterParentProp = value;
  }

  public get getterParentProp(): string | undefined {
    return this._getterSetterParentProp;
  }

  public set setterParentProp(value: string | undefined) {
    this._getterSetterParentProp = value;
  }

  public parentMethod(): string {
    return this.parentProp;
  }
}
