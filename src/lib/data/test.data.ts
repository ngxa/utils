import { EventEmitter } from '@angular/core';
import { asyncScheduler, EMPTY, of as just } from 'rxjs';

import { Dictionary } from '../dictionary/dictionary';

// tslint:disable:no-commented-code
// tslint:disable:no-construct
// tslint:disable:use-primitive-type
// tslint:disable:prefer-array-literal

/**
 * Maximum allowed Date value
 */
export const maxDateValue: number = 8640000000000000;

/**
 * Minimum allowed Date value
 */
export const minDateValue: number = -8640000000000000;

const valuesData: {} = {
  // Function
  Function: new Function(),

  // Object primitives
  Object_plain: new Object(),
  Object_Boolean: new Boolean(),
  Object_Symbol: Symbol(),
  Object_Number: new Number(),
  Object_String_Iterator: new String(),

  // Object Error
  Object_Error: new Error(),
  Object_EvalError: new EvalError(),
  Object_RangeError: new RangeError(),
  Object_ReferenceError: new ReferenceError(),
  Object_SyntaxError: new SyntaxError(),
  Object_TypeError: new TypeError(),
  Object_URIError: new URIError(),

  // Object RegExp
  Object_RegExp: new RegExp(''),

  // Object Array an Typed Array
  Object_Iterator_Array: new Array(),
  Object_Iterator_Tuple: ['0', 0],
  Object_Iterator_TypedArray_Int8Array: new Int8Array(0),
  Object_Iterator_TypedArray_Uint8Array: new Uint8Array(0),
  Object_Iterator_TypedArray_Uint8ClampedArray: new Uint8ClampedArray(0),
  Object_Iterator_TypedArray_Int16Array: new Int16Array(0),
  Object_Iterator_TypedArray_Uint16Array: new Uint16Array(0),
  Object_Iterator_TypedArray_Int32Array: new Int32Array(0),
  Object_Iterator_TypedArray_Uint32Array: new Uint32Array(0),
  Object_Iterator_TypedArray_Float32Array: new Float32Array(0),
  Object_Iterator_TypedArray_Float64Array: new Float64Array(0),

  // Object Collections
  Object_Iterator_Map: new Map(),
  Object_Iterator_Set: new Set(),
  Object_WeakMap: new WeakMap(),
  Object_WeakSet: new WeakSet(),

  // Object Buffer
  Object_ArrayBuffer: new ArrayBuffer(0),
  Object_DataView: new DataView(new ArrayBuffer(0)),

  // Object Promise
  Object_Promise: Promise.resolve(0),

  // Object Subscribable
  Object_Subscribable_Observable_async: just(0, asyncScheduler),
  Object_Subscribable_Observable_sync: just(0),
  Object_Subscribable_Observable_empty: EMPTY,
  Object_Subscribable_EventEmitter: new EventEmitter<number>(),

  // Object Date
  Object_Date: new Date(),
  Object_Date_max: new Date(maxDateValue),
  Object_Date_min: new Date(minDateValue),

  // Boolean
  boolean_false_falsy: false,
  boolean_true: true,
  boolean_BooleanConstructor_true_fromTrue: Boolean(true),
  boolean_BooleanConstructor_false_falsy_fromFalse: Boolean(false),
  boolean_BooleanConstructor_false_falsy_fromString: Boolean(''),
  boolean_BooleanConstructor_true_fromString: Boolean('a'),
  boolean_BooleanConstructor_false_falsy_fromNumber: Boolean(0),
  boolean_BooleanConstructor_false_falsy_fromNegativeNumber: Boolean(-0),
  boolean_BooleanConstructor_true_fromNumber: Boolean(1),
  boolean_BooleanConstructor_false_falsy_fromNaN: Boolean(NaN),
  boolean_BooleanConstructor_true_fromObject: Boolean({}),
  boolean_BooleanConstructor_false_falsy_fromNull: Boolean(null),
  boolean_BooleanConstructor_false_falsy_fromUndefined: Boolean(undefined),

  // Number
  number_finite_integer_zero_falsy_empty_positive: 0,
  number_finite_integer_zero_falsy_empty_negative: -0,
  number_finite_integer_positive: 1,
  number_finite_integer_negative: -1,
  number_finite_decimal_positive: 1.1,
  number_finite_decimal_negative: -1.1,
  number_finite_maximum: Number.MAX_VALUE,
  number_finite_minimum_negative: Number.MIN_VALUE,
  number_infinity: Infinity,
  number_infinity_negative: -Infinity,
  number_finite_binary: 0b1,
  number_finite_octal: 0o1,
  number_finite_hex: 0x1,
  number_nan_falsy_nullish: NaN,
  number_finite_constructor: Number(1),

  // String
  string_lowercase: 'a',
  string_uppercase: 'A',
  string_fromBoolean: 'true',
  string_fromNumber: '1',
  string_fromNull: 'null',
  string_fromObject: '{}',
  string_fromArray: '[]',
  string_fromUndefined: 'undefined',
  string_falsy_nullish_empty: '',
  string_space: ' ',
  string_constructor: String('a'),

  // Void
  undefined_falsy_nullish_empty: undefined,
  object_null_falsy_nullish_empty: null
};

/**
 * A set of basic data to help in test.
 */
export const testData: Dictionary<string, any> = new Dictionary(valuesData);
