/**
 * Grand test class
 */
export class GrandTestClass {
  public grandProp: string = 'g';
  public undefinedGrandProp!: string;
  public nullGrandProp: any = null;
  private _getterSetterGrandProp?: string;

  public get getterSetterGrandProp(): string | undefined {
    return this._getterSetterGrandProp;
  }

  public set getterSetterGrandProp(value: string | undefined) {
    this._getterSetterGrandProp = value;
  }

  public get getterGrandProp(): string | undefined {
    return this._getterSetterGrandProp;
  }

  public set setterGrandProp(value: string | undefined) {
    this._getterSetterGrandProp = value;
  }

  public grandMethod(): string {
    return this.grandProp;
  }
}
