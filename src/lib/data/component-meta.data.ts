import { ChangeDetectionStrategy } from '@angular/core';

/**
 * Basic content for @component metadata.
 */
export const componentMeta: any = {
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'ngxrb-test',
  template: '<div></div>'
};
