import { SimpleChanges } from '@angular/core';

import { isVoid } from './typings/is-void';

/**
 * Check that the input has changed.
 *
 * @param input The input to test.
 * @param changes The `SimpleChanges` object.
 * @returns true if the input exist and have changed.
 */
export function inputChange(input: string, changes: SimpleChanges): boolean {
  return !isVoid(changes[input]) && changes[input].previousValue !== changes[input].currentValue;
}
