import { sameValueZero } from './same-value-zero';

describe('sameValueZero(value1, value2)', () => {
  test('returns true for undefined / undefined', () => {
    expect(sameValueZero(undefined, undefined)).toEqual(true);
  });

  test('returns true for null / null', () => {
    expect(sameValueZero(null, null)).toEqual(true);
  });

  test('returns true for true / true', () => {
    expect(sameValueZero(true, true)).toEqual(true);
  });

  test('returns true for false / false', () => {
    expect(sameValueZero(false, false)).toEqual(true);
  });

  test('returns true for "foo" / "foo"', () => {
    expect(sameValueZero('foo', 'foo')).toEqual(true);
  });

  test('returns true for 0 / 0', () => {
    expect(sameValueZero(0, 0)).toEqual(true);
  });

  test('returns true for +0 / -0', () => {
    expect(sameValueZero(+0, -0)).toEqual(true);
  });

  test('returns true for +0 / 0', () => {
    expect(sameValueZero(+0, 0)).toEqual(true);
  });

  test('returns true for -0 / 0', () => {
    expect(sameValueZero(-0, 0)).toEqual(true);
  });

  test('returns true for NaN / NaN', () => {
    expect(sameValueZero(NaN, NaN)).toEqual(true);
  });

  test('returns false for 0 / false', () => {
    expect(sameValueZero(0, false)).toEqual(false);
  });

  test('returns false for "" / false', () => {
    expect(sameValueZero('', false)).toEqual(false);
  });

  test('returns false for "" / 0', () => {
    expect(sameValueZero('', 0)).toEqual(false);
  });

  test('returns false for "0" / 0', () => {
    expect(sameValueZero('0', 0)).toEqual(false);
  });

  test('returns false for "17" / 17', () => {
    expect(sameValueZero('17', 17)).toEqual(false);
  });

  test('returns false for [1, 2] / "1,2"', () => {
    expect(sameValueZero([1, 2], '1,2')).toEqual(false);
  });

  test('returns false for new String("foo") / "foo"', () => {
    // tslint:disable:no-construct
    // tslint:disable-next-line:use-primitive-type
    expect(sameValueZero(new String('foo'), 'foo')).toEqual(false);
  });

  test('returns false for null / undefined', () => {
    expect(sameValueZero(null, undefined)).toEqual(false);
  });

  test('returns false for null / false', () => {
    expect(sameValueZero(null, false)).toEqual(false);
  });

  test('returns false for undefined / false', () => {
    expect(sameValueZero(undefined, false)).toEqual(false);
  });

  test('returns false for { foo: "bar" } / { foo: "bar" }', () => {
    expect(sameValueZero({ foo: 'bar' }, { foo: 'bar' })).toEqual(false);
  });

  test('returns false for new String("foo") / new String("foo")', () => {
    // tslint:disable:no-construct
    // tslint:disable-next-line:use-primitive-type
    expect(sameValueZero(new String('foo'), new String('foo'))).toEqual(false);
  });

  test('returns false for 0 / null', () => {
    expect(sameValueZero(0, null)).toEqual(false);
  });

  test('returns false for 0 / NaN', () => {
    expect(sameValueZero(0, NaN)).toEqual(false);
  });

  test('returns false for "foo" / NaN', () => {
    expect(sameValueZero('foo', NaN)).toEqual(false);
  });
});
