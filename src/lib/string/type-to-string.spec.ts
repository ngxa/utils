import { of } from 'rxjs';

import { TestClass } from '../data/test-class.data';
import { isVoid } from '../typings/is-void';

import { typeToString } from './type-to-string';

describe('typeToString(value)', () => {
  it('returns "string" for strings', () => {
    expect(typeToString('a')).toEqual('string');
  });

  it('returns "number" for numbers', () => {
    expect(typeToString(0)).toEqual('number');
  });

  it('returns "boolean" for booleans', () => {
    expect(typeToString(true)).toEqual('boolean');
  });

  it('returns "" for anonymous functions', () => {
    expect(typeToString(() => 'a')).toEqual('');
  });

  it('returns "ZoneAwarePromise" for promises', () => {
    expect(typeToString(Promise.resolve(0))).toEqual('ZoneAwarePromise');
  });

  it('returns "Observable" for observables', () => {
    expect(typeToString(of('a'))).toEqual('Observable');
  });

  it('returns function name', () => {
    expect(typeToString(isVoid)).toEqual('isVoid');
  });

  it('returns class name', () => {
    const obj: TestClass = new TestClass();

    expect(typeToString(obj)).toEqual('TestClass');
  });
});
