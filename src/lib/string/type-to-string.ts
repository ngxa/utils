/**
 * Returns the value type or name as string.
 *
 * @param value The value to check.
 * @returns The value type or name as string.
 */
export function typeToString(value: any): string {
  return typeof value === 'function'
    ? value.name
    : typeof value === 'object'
    ? value.constructor.name
    : typeof value;
}
