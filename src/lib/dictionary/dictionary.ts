import { arrayIncludes } from '../array/array-includes';
import { objectEntries } from '../object/object-entries';
import { ObjectLiteral } from '../object/types/object-literal.interface';
import { isVoid } from '../typings/is-void';
import { DictionaryKey } from './types/dictionary-key.type';
import { DictionaryLike } from './types/dictionary-like.type';
import { Entry } from './types/entry.type';
import { reduceDictionary } from './utils/reduce-dictionary';

/**
 * A dictionary of key-value pairs.
 */
export class Dictionary<K, V> {
  /**
   * Creates a dictionary from a dictionary-like object.
   * @param dictionaryLike A dictionary-like object.
   * @returns A dictionary with the dictionary-like values.
   */
  static from<T, U>(dictionaryLike: DictionaryLike<T, U>): Dictionary<T, U> {
    return new Dictionary(dictionaryLike);
  }

  /**
   * Determines whether the passed value is a dictionary.
   * @param arg The object to be checked.
   * @returns `true` if the object is a Dictionary, if not `false`.
   */
  static isDictionary<T, U>(arg: any): arg is Dictionary<T, U> {
    return typeof arg === 'object' && arg instanceof Dictionary;
  }

  /**
   * Creates a new dictionary with a variable number of entries.
   * @param entries The entries of which to create the dictionary.
   * @returns A dictionary with the entries.
   */
  static of<T, U>(...entries: Entry<T, U>[]): Dictionary<T, U> {
    return new Dictionary(entries);
  }

  private _value: Map<DictionaryKey<K>, V>;

  /**
   * Creates a dictionary of key-value pairs.
   * @param [dictionaryLike] A dictionary-like object.
   */
  constructor(dictionaryLike?: DictionaryLike<K, V>) {
    this._value =
      dictionaryLike === undefined
        ? new Map()
        : dictionaryLike instanceof Map
        ? dictionaryLike
        : dictionaryLike instanceof Dictionary
        ? dictionaryLike.toMap()
        : new Map(dictionaryLike instanceof Array ? dictionaryLike : objectEntries(dictionaryLike));
  }

  /**
   * Returns the number of entries in the dictionary.
   */
  get size(): number {
    return this._value.size;
  }

  /**
   * Removes all entries from the dictionary.
   */
  clear(): void {
    this._value.clear();
  }

  /**
   * Concatenates dictionary-like objects to the calling dictionary.
   * @param dictionaryLikes The dictionary-like objects to concatenate.
   * @returns The combined dictionary.
   */
  concat(...dictionaryLikes: DictionaryLike<K, V>[]): Dictionary<K, V> {
    return new Dictionary(
      dictionaryLikes.reduce(
        (entry: Entry<K, V>[], dictionaryLike: DictionaryLike<K, V>) =>
          entry.concat(new Dictionary(dictionaryLike).toArray()),
        Array.from(this._value.entries())
      )
    );
  }

  /**
   * Removes the specified entry from the dictionary.
   * @param key The key of the entry to remove.
   * @returns `true` if the key exist and has been removed, if not `false`.
   */
  delete(key: DictionaryKey<K>): boolean {
    return this._value.delete(key);
  }

  /**
   * Returns an iterator of entries in the dictionary.
   * @returns An iterator with all entries.
   */
  entries(): IterableIterator<Entry<K, V>> {
    return this._value.entries();
  }

  /**
   * Determines whether all the entries of a dictionary satisfy the specified test.
   * @param callbackfn A function that accepts up to two arguments.
   * @returns `true` if returns a truthy value for every entry, if not `false`.
   */
  every(callbackfn: (entry: Entry<K, V>, dictionary: Dictionary<K, V>) => any): boolean {
    const iterator: IterableIterator<Entry<K, V>> = this._value.entries();
    let next: IteratorResult<Entry<K, V>> = iterator.next();
    let isEvery: boolean = !next.done;

    while (isEvery && !next.done) {
      isEvery = Boolean(callbackfn(next.value, this));
      next = iterator.next();
    }

    return isEvery;
  }

  /**
   * Returns a dictionary with the entries that meets the condition specified in the function.
   * @param callbackfn A function that accepts up to two arguments.
   * @returns A dictionary with the entries that pass the test.
   */
  filter(callbackfn: (entry: Entry<K, V>, dictionary: Dictionary<K, V>) => any): Dictionary<K, V> {
    return new Dictionary(
      Array.from(this._value.entries()).filter((entry: Entry<K, V>) => callbackfn(entry, this))
    );
  }

  /**
   * Executes a provided function once per each entry, in order.
   * @param callbackfn A function to execute for each entry.
   */
  forEach(callbackfn: (entry: Entry<K, V>, dictionary: Dictionary<K, V>) => void): void {
    Array.from(this._value.entries()).forEach((entry: Entry<K, V>) => {
      callbackfn(entry, this);
    });
  }

  /**
   * Returns the value of the specified entry.
   * @param key The key of the entry to return.
   * @returns The value associated with the key or `undefined` if the key can't be found.
   */
  get(key: DictionaryKey<K>): V | undefined {
    return this._value.get(key);
  }

  /**
   * Check if an entry with the specified key exists or not.
   * @param key The key of the entry to test for presence.
   * @returns `true` if the specified key exists, if not `false`.
   */
  has(key: DictionaryKey<K>): boolean {
    return this._value.has(key);
  }

  /**
   * Returns an iterable of keys in the dictionary.
   * @returns An iterable of keys.
   */
  keys(): IterableIterator<DictionaryKey<K>> {
    return this._value.keys();
  }

  /**
   * Calls a defined function once per each entry and returns a dictionary with the results.
   * @param callbackfn A function that accepts up to two arguments.
   * @returns A dictionary with each entry being the result of the function.
   */
  map<T, U>(
    callbackfn: (entry: Entry<K, V>, dictionary: Dictionary<K, V>) => Entry<T, U>
  ): Dictionary<T, U> {
    return new Dictionary(
      Array.from(this._value.entries()).map((_entry: Entry<K, V>) => callbackfn(_entry, this))
    );
  }

  /**
   * Returns the result of filter the dictionary keys that match the RegExp expressions.
   * @param [regexps] RegExp expressions. If multiple expressions are provided in a call, they will
   * behave as logical OR. If no expression is provided will return an empty dictionary.
   * @returns A dictionary containing the results of the match.
   */
  match(...regexps: string[]): Dictionary<K, V> {
    let result: Entry<K, V>[] = [];

    regexps.forEach(
      (regexp: string) =>
        (result = result.concat(
          Array.from(this._value.entries()).filter((entry: Entry<K, V>) =>
            RegExp(regexp).test(entry[0].toString())
          )
        ))
    );

    return new Dictionary(result);
  }

  /**
   * Returns the result of filter the dictionary keys that do not match the RegExp expressions.
   * @param [regexps] RegExp expressions. If multiple expressions are provided in a call, they will
   * behave as logical AND. If no expression is provided will return the original dictionary.
   * @returns A dictionary containing the results of the mismatch.
   */
  mismatch(...regexps: string[]): Dictionary<K, V> {
    const [head, ...tail]: string[] = regexps;
    const keys: DictionaryKey<K>[] = Array.from(this.keys());
    let index: number = 0;

    let filtered: DictionaryKey<K>[] = !isVoid(head)
      ? keys.filter((key: DictionaryKey<K>) => RegExp(head).test(key.toString()))
      : keys;

    while (filtered.length > 0 && index < tail.length) {
      filtered = filtered.filter((key: DictionaryKey<K>) =>
        RegExp(tail[index]).test(key.toString())
      );
      index += 1;
    }

    return filtered !== keys
      ? new Dictionary(
          Array.from(this._value.entries()).filter(
            (entry: Entry<K, V>) => !arrayIncludes(filtered, entry[0])
          )
        )
      : this;
  }

  /**
   * Removes the last entry and returns it as dictionary.
   * @returns A dictionary with the removed entry, or `undefined` if is empty.
   */
  pop(): Dictionary<K, V> | undefined {
    return this._removeAndReturn(Array.from(this.keys()).pop());
  }

  /**
   * Calls the specified function for all the entries. The return value of the function is the
   * accumulated result, and is provided as an argument in the next call to the function.
   * @param callbackfn A function that accepts up to three arguments.
   * @param [initialValue] If is specified, it is used as the initial value to start the
   * accumulation. The first call to the callbackfn function provides this value as an argument.
   * @returns The value that results from the reduction.
   */
  reduce(
    callbackfn: (
      previousValue: Entry<K, V>,
      currentValue: Entry<K, V>,
      dictionary: Dictionary<K, V>
    ) => Entry<K, V>,
    initialValue?: Entry<K, V>
  ): Entry<K, V>;

  /**
   * Calls the specified function for all the entries. The return value of the function is the
   * accumulated result, and is provided as an argument in the next call to the function.
   * @param callbackfn A function that accepts up to three arguments.
   * @param initialValue It is used as the initial value to start the accumulation. The first call
   * to the function provides this value as an argument.
   * @returns The value that results from the reduction.
   */
  reduce<T>(
    callbackfn: (previousValue: T, currentValue: Entry<K, V>, dictionary: Dictionary<K, V>) => T,
    initialValue: T
  ): T;

  /**
   * Calls the specified function for all the entries. The return value of the function is the
   * accumulated result, and is provided as an argument in the next call to the function.
   * @param callbackfn A function that accepts up to three arguments.
   * @param [initialValue] If is specified, it is used as the initial value to start the
   * accumulation. The first call to the callbackfn function provides this value as an argument
   * instead of an entry value.
   * @returns The value that results from the reduction.
   */
  reduce<T>(
    callbackfn: (
      previousValue: T | Entry<K, V>,
      currentValue: Entry<K, V>,
      dictionary: Dictionary<K, V>
    ) => T | Entry<K, V>,
    initialValue?: T
  ): T | Entry<K, V> {
    return reduceDictionary(this, callbackfn, initialValue);
  }

  /**
   * Calls the specified function for all the entries in descending order. The return value of the
   * function is the accumulated result, and is provided as an argument in the next call to the
   * function.
   * @param callbackfn A function that accepts up to three arguments.
   * @param [initialValue] If is specified, it is used as the initial value to start the
   * accumulation. The first call to the callbackfn function provides this value as an argument.
   * @returns The value that results from the reduction.
   */
  reduceRight(
    callbackfn: (
      previousValue: Entry<K, V>,
      currentValue: Entry<K, V>,
      dictionary: Dictionary<K, V>
    ) => Entry<K, V>,
    initialValue?: Entry<K, V>
  ): Entry<K, V>;

  /**
   * Calls the specified function for all the entries in descending order. The return value of the
   * function is the accumulated result, and is provided as an argument in the next call to the
   * function.
   * @param callbackfn A function that accepts up to three arguments.
   * @param initialValue It is used as the initial value to start the accumulation. The first call
   * to the function provides this value as an argument.
   * @returns The value that results from the reduction.
   */
  reduceRight<T>(
    callbackfn: (previousValue: T, currentValue: Entry<K, V>, dictionary: Dictionary<K, V>) => T,
    initialValue: T
  ): T;

  /**
   * Calls the specified function for all the entries in descending order. The return value of the
   * function is the accumulated result, and is provided as an argument in the next call to the
   * function.
   * @param callbackfn A function that accepts up to three arguments.
   * @param [initialValue] If is specified, it is used as the initial value to start the
   * accumulation. The first call to the callbackfn function provides this value as an argument
   * instead of an entry value.
   * @returns The value that results from the reduction.
   */
  reduceRight<T>(
    callbackfn: (
      previousValue: T | Entry<K, V>,
      currentValue: Entry<K, V>,
      dictionary: Dictionary<K, V>
    ) => T | Entry<K, V>,
    initialValue?: T
  ): T | Entry<K, V> {
    return reduceDictionary(
      new Dictionary(this._mapValues(Array.from(this.keys()).reverse())),
      callbackfn,
      initialValue
    );
  }

  /**
   * Reverses the entries order.
   * @returns The reversed dictionary.
   */
  reverse(): this {
    this._value = this._mapValues(Array.from(this.keys()).reverse());

    return this;
  }

  /**
   * Adds or updates an entry with the specified key and value.
   * @param key The key of the entry to add or update.
   * @param value The value of the entry to add or update.
   * @returns The modified dictionary.
   */
  set(key: DictionaryKey<K>, value: V): this {
    this._value.set(key, value);

    return this;
  }

  /**
   * Removes the first entry and returns it as dictionary.
   * @returns A dictionary with the removed entry, or `undefined` if is empty.
   */
  shift(): Dictionary<K, V> | undefined {
    return this._removeAndReturn(Array.from(this.keys()).shift());
  }

  /**
   * Determines whether any entry satisfy the specified test.
   * @param callbackfn A function that accepts up to two arguments.
   * @returns `true` if returns a truthy value for any entry, if not `false`.
   */
  some(callbackfn: (entry: Entry<K, V>, dictionary: Dictionary<K, V>) => any): boolean {
    const iterator: IterableIterator<Entry<K, V>> = this._value.entries();
    let next: IteratorResult<Entry<K, V>> = iterator.next();
    let isSome: boolean = false;

    while (!isSome && !next.done) {
      isSome = Boolean(callbackfn(next.value, this));
      next = iterator.next();
    }

    return isSome;
  }

  /**
   * Sorts a dictionary by key.
   * @param compareFn The function used to determine the order. If omitted, the keys are sorted in
   * ascending, ASCII character order.
   * @returns The sorted dictionary.
   */
  sort(compareFn?: (valueA: DictionaryKey<K>, valueB: DictionaryKey<K>) => number): this {
    this._value = this._mapValues(Array.from(this._value.keys()).sort(compareFn));

    return this;
  }

  /**
   * Returns an Array with the entries.
   * @returns An Array with the entries.
   */
  toArray(): Entry<K, V>[] {
    return Array.from(this._value.entries());
  }

  /**
   * Returns a Map with the entries.
   * @returns A Map with the entries.
   */
  toMap(): Map<DictionaryKey<K>, V> {
    return this._value;
  }

  /**
   * Returns an Object with the entries.
   * @returns An Object with the entries.
   */
  toObject(): {} {
    const obj: ObjectLiteral = Object.create(null);

    this._value.forEach((value: V, key: DictionaryKey<K>) => (obj[key.toString()] = value));

    return obj;
  }

  /**
   * Returns an iterable of values.
   * @returns An iterable of values.
   */
  values(): IterableIterator<V> {
    return this._value.values();
  }

  private _removeAndReturn(key: DictionaryKey<K> | undefined): Dictionary<K, V> | undefined {
    if (key !== undefined) {
      const value: V | undefined = this.get(key);

      this._value.delete(key);

      return new Dictionary([[key, value as V]]);
    }
  }

  private _mapValues(keys: DictionaryKey<K>[]): Map<DictionaryKey<K>, V> {
    return keys.reduce(
      (map: Map<DictionaryKey<K>, V>, key: DictionaryKey<K>) =>
        map.set(key, this._value.get(key) as V),
      new Map()
    );
  }
}
