import { Dictionary } from './dictionary';
import { Entry } from './types/entry.type';
import { reduceEntry, reduceNumber } from './utils/reduce-dictionary.spec';

// tslint:disable:max-file-line-count

const objData: {} = { aAA: 1, aAB: 2, aC: 3, aD: 4, bA: 5, bB: 6, zcC: 7, dAD: 8 };
const entriesData: Entry<string, number>[] = [
  ['aAA', 1],
  ['aAB', 2],
  ['aC', 3],
  ['aD', 4],
  ['bA', 5],
  ['bB', 6],
  ['zcC', 7],
  ['dAD', 8]
];
const keysData: string[] = ['aAA', 'aAB', 'aC', 'aD', 'bA', 'bB', 'zcC', 'dAD'];
const valuesData: number[] = [1, 2, 3, 4, 5, 6, 7, 8];
const [headData, secondData, ...tailData] = entriesData;

function trueTest(entry: Entry<string, number>): boolean {
  return entry[0].toString().length < 5;
}

function falseTest(entry: Entry<string, number>): boolean {
  return entry[0].toString().length < 2;
}

function someTest(entry: Entry<string, number>): boolean {
  return entry[0].toString().length < 3;
}

function mapFn(entry: Entry<string, number>): Entry<string, string> {
  return [`N${entry[0]}`, (entry[1] + 1).toString()];
}

function compareFn(valueA: string, valueB: string): number {
  if (valueB > valueA) {
    return 1;
  }
  if (valueB < valueA) {
    return -1;
  }

  return 0;
}

// tslint:disable-next-line:no-big-function
describe('Dictionary(dictionaryLike?)', () => {
  let mapData: Map<string, number>;
  let emptyDict: Dictionary<string, number>;
  let entriesDict: Dictionary<string, number>;
  let mapDict: Dictionary<string, number>;
  let objDict: Dictionary<string, number>;
  let dict: Dictionary<string, number>;
  let filterKDict: Dictionary<string, number>;
  let filterVDict: Dictionary<string, number>;
  let filterFalseDict: Dictionary<string, number>;
  let headDict: Dictionary<string, number>;
  let secondDict: Dictionary<string, number>;
  let tailDict: Dictionary<string, number>;
  let lastDict: Dictionary<string, number>;

  beforeEach(() => {
    mapData = new Map(entriesData);
    emptyDict = new Dictionary();
    entriesDict = new Dictionary(entriesData);
    mapDict = new Dictionary(mapData);
    objDict = new Dictionary(objData);
    dict = new Dictionary(objDict);
    filterKDict = objDict.filter((entry: Entry<string, number>) =>
      entry[0].toString().startsWith('a')
    );
    filterVDict = objDict.filter((entry: Entry<string, number>) => entry[1] <= 3);
    filterFalseDict = objDict.filter((entry: Entry<string, number>) => entry[1] < 1);
    headDict = Dictionary.of(headData);
    secondDict = Dictionary.of(secondData);
    tailDict = Dictionary.of(...tailData);
    lastDict = Dictionary.of(['dAD', 8]);
  });

  test('Dictionary.from() returns a dictionary instance', () => {
    expect(Dictionary.from(emptyDict)).toEqual(emptyDict);
    expect(Dictionary.from(entriesData)).toEqual(entriesDict);
    expect(Dictionary.from(mapData)).toEqual(mapDict);
    expect(Dictionary.from(objData)).toEqual(objDict);
    expect(Dictionary.from(objDict)).toEqual(objDict);
  });

  test('Dictionary.isDictionary() returns true for a dictionary instance', () => {
    expect(Dictionary.isDictionary(emptyDict)).toBeTruthy();
    expect(Dictionary.isDictionary(entriesDict)).toBeTruthy();
    expect(Dictionary.isDictionary(mapDict)).toBeTruthy();
    expect(Dictionary.isDictionary(objDict)).toBeTruthy();
    expect(Dictionary.isDictionary(dict)).toBeTruthy();
  });

  test('Dictionary.isDictionary() returns false for other types', () => {
    expect(Dictionary.isDictionary(entriesData)).toBeFalsy();
    expect(Dictionary.isDictionary(mapData)).toBeFalsy();
    expect(Dictionary.isDictionary(objData)).toBeFalsy();
    expect(Dictionary.isDictionary(1)).toBeFalsy();
    expect(Dictionary.isDictionary('')).toBeFalsy();
    expect(Dictionary.isDictionary(true)).toBeFalsy();
    expect(Dictionary.isDictionary([])).toBeFalsy();
    expect(Dictionary.isDictionary({})).toBeFalsy();
    expect(Dictionary.isDictionary(new Date())).toBeFalsy();
  });

  test('Dictionary.of() returns Dictionary wtesthout args', () => {
    expect(Dictionary.of()).toEqual(emptyDict);
  });

  test('Dictionary.of() returns Dictionary from simple Tuple', () => {
    expect(Dictionary.of(headData)).toEqual(new Dictionary([headData]));
  });

  test('Dictionary.of() returns Dictionary from multiple Tuples', () => {
    expect(Dictionary.of(...entriesData)).toEqual(entriesDict);
  });

  test('instantiated wtesthout args', () => {
    expect(emptyDict.toObject()).toEqual({});
  });

  test('instantiated wtesth empty Tuple Array', () => {
    expect(new Dictionary([]).toObject()).toEqual({});
  });

  test('instantiated wtesth Tuple Array', () => {
    expect(entriesDict.toObject()).toEqual(objData);
  });

  test('instantiated wtesth empty Map', () => {
    expect(new Dictionary(new Map()).toObject()).toEqual({});
  });

  test('instantiated wtesth Map', () => {
    expect(mapDict.toObject()).toEqual(objData);
  });

  test('instantiated wtesth empty Object', () => {
    expect(new Dictionary({}).toObject()).toEqual({});
  });

  test('instantiated wtesth Object', () => {
    expect(objDict.toObject()).toEqual(objData);
  });

  test('instantiated wtesth empty Dictionary', () => {
    expect(new Dictionary(new Dictionary()).toObject()).toEqual({});
  });

  test('instantiated wtesth Dictionary', () => {
    expect(dict.toObject()).toEqual(objData);
  });

  test('.size returns the number of key-value pairs', () => {
    expect(emptyDict.size).toEqual(0);
    expect(objDict.size).toEqual(8);
    expect(filterKDict.size).toEqual(4);
  });

  test('.clear() removes all key-value pairs', () => {
    expect(objDict.toObject()).toEqual(objData);
    objDict.clear();
    expect(objDict).toEqual(emptyDict);
  });

  test('.clear() removes all key-value pairs from empty dictionary', () => {
    expect(emptyDict.toObject()).toEqual({});
    emptyDict.clear();
    expect(emptyDict).toEqual(emptyDict);
  });

  test('.concat() returns concatenated wtesthout args', () => {
    expect(headDict.concat()).toEqual(headDict);
  });

  test('.concat() returns concatenated single dictionary', () => {
    expect(headDict.concat(secondDict)).toEqual(Dictionary.of(headData, secondData));
  });

  test('.concat() returns concatenated multiple dictionaries', () => {
    expect(headDict.concat(secondDict, tailDict)).toEqual(entriesDict);
    expect(headDict.concat(secondDict).concat(tailDict)).toEqual(entriesDict);
  });

  test('.concat() returns concatenated from empty dictionary', () => {
    expect(emptyDict.concat(headDict)).toEqual(headDict);
  });

  test('.concat() returns concatenated wtesthout duplicated keys', () => {
    expect(objDict.concat(headDict)).toEqual(objDict);
  });

  test('.delete() returns true if key exist', () => {
    expect(objDict.delete('aAA')).toEqual(true);
  });

  test('.delete() returns false if key do not exist', () => {
    expect(objDict.delete('notAKey')).toEqual(false);
    expect(emptyDict.delete('aAA')).toEqual(false);
  });

  test('.delete() remove the key-value pair', () => {
    objDict.delete('aAA');
    expect(Array.from(objDict.values())).toEqual([2, 3, 4, 5, 6, 7, 8]);
  });

  test('.entries() returns an testerable of key-value pairs', () => {
    expect(mapDict.entries()).toEqual(mapData.entries());
  });

  test('.every() returns true if every members satisfy the test', () => {
    expect(mapDict.every(trueTest)).toBeTruthy();
  });

  test('.every() returns false if every members do not satisfy the test', () => {
    expect(mapDict.every(falseTest)).toBeFalsy();
  });

  test('.every() returns false if some members do not satisfy the test', () => {
    expect(mapDict.every(someTest)).toBeFalsy();
  });

  test('.every() returns false if the dictionary is empty', () => {
    expect(emptyDict.every(trueTest)).toBeFalsy();
  });

  test('.filter() returns a dictionary wtesth the key-value pairs that meets the condtestion', () => {
    expect(Array.from(filterKDict.values())).toEqual([1, 2, 3, 4]);
    expect(Array.from(filterVDict.values())).toEqual([1, 2, 3]);
  });

  test('.filter() returns an empty dictionary if no key-value pair meets the condtestion', () => {
    expect(Array.from(filterFalseDict.values())).toEqual([]);
  });

  test('.forEach() executes a provided function once per each key-value pair', () => {
    let length: number = 0;

    objDict.forEach((entry: [string, number], dictionary: Dictionary<string, number>) => {
      expect(entry).toBeDefined();
      expect(dictionary).toBeDefined();
      expect(dictionary).toEqual(objDict);
      expect(dictionary.has(entry[0])).toBeTruthy();
      expect(dictionary.get(entry[0])).toEqual(entry[1]);
      length += 1;
    });
    expect(length).toEqual(objDict.size);
  });

  test('.get() returns the value asociated to the key', () => {
    expect(objDict.get('aAA')).toEqual(1);
  });

  test('.get() returns undefined if does not exist', () => {
    expect(objDict.get('notAKey')).toBeUndefined();
  });

  test('.has() returns true if key exist', () => {
    expect(objDict.has('aAA')).toBeTruthy();
  });

  test('.has() returns false if key do not exist', () => {
    expect(objDict.has('notAKey')).toBeFalsy();
  });

  test('.keys() returns Array of keys', () => {
    expect(Array.from(emptyDict.keys())).toEqual([]);
    expect(Array.from(entriesDict.keys())).toEqual(keysData);
    expect(Array.from(mapDict.keys())).toEqual(keysData);
    expect(Array.from(objDict.keys())).toEqual(keysData);
    expect(Array.from(dict.keys())).toEqual(keysData);
  });

  test('.map() returns the dictionary of results', () => {
    expect(Array.from(filterVDict.map(mapFn).keys())).toEqual(['NaAA', 'NaAB', 'NaC']);
    expect(Array.from(filterVDict.map(mapFn).values())).toEqual(['2', '3', '4']);
  });

  test('.match() returns empty if empty dictionary', () => {
    expect(Array.from(emptyDict.match('a').keys())).toEqual([]);
  });

  test('.match() returns empty if empty regexp', () => {
    expect(Array.from(objDict.match().keys())).toEqual([]);
  });

  test('.match() returns empty if no match', () => {
    expect(Array.from(objDict.match('e').keys())).toEqual([]);
  });

  test('.match() returns filtered wtesth single regexp', () => {
    expect(Array.from(objDict.match('a').keys())).toEqual(['aAA', 'aAB', 'aC', 'aD']);
    expect(Array.from(objDict.match('aA').keys())).toEqual(['aAA', 'aAB']);
    expect(Array.from(objDict.match('A$').keys())).toEqual(['aAA', 'bA']);
  });

  test('.match() returns filtered wtesth multiple regexp as OR', () => {
    expect(Array.from(objDict.match('d', 'z').keys())).toEqual(['dAD', 'zcC']);
  });

  test('.match() returns filtered wtesth multiple calls as AND', () => {
    expect(
      Array.from(
        objDict
          .match('a')
          .match('A')
          .keys()
      )
    ).toEqual(['aAA', 'aAB']);
  });

  test('.match() returns filtered wtesth multiple regexp and calls', () => {
    expect(
      Array.from(
        objDict
          .match('d', 'z')
          .match('A')
          .keys()
      )
    ).toEqual(['dAD']);
  });

  test('.mismatch() returns empty if empty dictionary', () => {
    expect(Array.from(emptyDict.mismatch('a').keys())).toEqual([]);
  });

  test('.mismatch() returns original dictionary if empty regexp', () => {
    expect(Array.from(objDict.mismatch().keys())).toEqual(keysData);
  });

  test('.mismatch() returns original dictionary if no match', () => {
    expect(Array.from(objDict.mismatch('e').keys())).toEqual(keysData);
  });

  test('.mismatch() returns filtered wtesth single regexp', () => {
    expect(Array.from(objDict.mismatch('a').keys())).toEqual(['bA', 'bB', 'zcC', 'dAD']);
    expect(Array.from(objDict.mismatch('aA').keys())).toEqual([
      'aC',
      'aD',
      'bA',
      'bB',
      'zcC',
      'dAD'
    ]);
    expect(Array.from(objDict.mismatch('A$').keys())).toEqual([
      'aAB',
      'aC',
      'aD',
      'bB',
      'zcC',
      'dAD'
    ]);
  });

  test('.mismatch() returns filtered wtesth multiple regexp as AND', () => {
    expect(Array.from(objDict.mismatch('a', 'A').keys())).toEqual([
      'aC',
      'aD',
      'bA',
      'bB',
      'zcC',
      'dAD'
    ]);
  });

  test('.mismatch() returns filtered wtesth multiple regexp and calls', () => {
    expect(
      Array.from(
        objDict
          .mismatch('a', 'A')
          .mismatch('b')
          .keys()
      )
    ).toEqual(['aC', 'aD', 'zcC', 'dAD']);
  });

  test('.pop() returns a dictionary wtesth the last element', () => {
    expect(objDict.pop()).toEqual(lastDict);
  });

  test('.pop() returns undefined if dictionary is empty', () => {
    expect(emptyDict.pop()).toBeUndefined();
  });

  test('.pop() remove the last element from the dictionary', () => {
    objDict.pop();
    expect(Array.from(objDict.values())).toEqual([1, 2, 3, 4, 5, 6, 7]);
  });

  test('.reduce() returns the accumulated value', () => {
    expect(objDict.reduce(reduceEntry)).toEqual(['all', -34]);
    expect(objDict.reduce(reduceEntry, ['', 0])).toEqual(['all', -36]);
  });

  test('.reduce() returns the accumulated wtesth another type', () => {
    expect(objDict.reduce(reduceNumber, 0)).toEqual(-36);
  });

  test('.reduceRight() returns the accumulated value', () => {
    expect(objDict.reduceRight(reduceEntry)).toEqual(['all', -20]);
    expect(objDict.reduceRight(reduceEntry, ['', 0])).toEqual(['all', -36]);
  });

  test('.reduceRight() returns the accumulated wtesth another type', () => {
    expect(objDict.reduceRight(reduceNumber, 0)).toEqual(-36);
  });

  test('.reverse() returns the reversed dictionary', () => {
    expect(Array.from(objDict.reverse().values())).toEqual([8, 7, 6, 5, 4, 3, 2, 1]);
  });

  test('.reverse() reverse the dictionary', () => {
    objDict.reverse();
    expect(Array.from(objDict.values())).toEqual([8, 7, 6, 5, 4, 3, 2, 1]);
  });

  test('.set() returns the dictionary wtesth the new key-value pair', () => {
    expect(Array.from(emptyDict.values())).toEqual([]);
    expect(Array.from(emptyDict.set('aAA', 0).values())).toEqual([0]);
  });

  test('.set() add a key-value pair to an empty dictionary', () => {
    expect(Array.from(emptyDict.values())).toEqual([]);
    emptyDict.set('aAA', 0);
    expect(Array.from(emptyDict.values())).toEqual([0]);
  });

  test('.set() add a key-value pair to a non empty dictionary', () => {
    expect(Array.from(emptyDict.values())).toEqual([]);
    emptyDict.set('aAA', 0);
    emptyDict.set('bBB', 1);
    expect(Array.from(emptyDict.values())).toEqual([0, 1]);
  });

  test('.set() override existing keys', () => {
    expect(Array.from(emptyDict.values())).toEqual([]);
    expect(Array.from(emptyDict.set('aAA', 0).values())).toEqual([0]);
    expect(Array.from(emptyDict.set('aAA', 1).values())).toEqual([1]);
  });

  test('.shift() returns a dictionary wtesth the first element', () => {
    expect(objDict.shift()).toEqual(headDict);
  });

  test('.shift() returns undefined if dictionary is empty', () => {
    expect(emptyDict.shift()).toBeUndefined();
  });

  test('.shift() remove the first element from the dictionary', () => {
    objDict.shift();
    expect(Array.from(objDict.values())).toEqual([2, 3, 4, 5, 6, 7, 8]);
  });

  test('.some() returns true if every member satisfy the test', () => {
    expect(mapDict.some(trueTest)).toBeTruthy();
  });

  test('.some() returns false if every members does not satisfy the test', () => {
    expect(mapDict.some(falseTest)).toBeFalsy();
  });

  test('.some() returns true if some members satisfy the test', () => {
    expect(mapDict.some(someTest)).toBeTruthy();
  });

  test('.some() returns false if the dictionary is emptyDict', () => {
    expect(emptyDict.some(trueTest)).toBeFalsy();
  });

  test('.sort() returns the sorted Dictionary wtesthout compareFn', () => {
    expect(Array.from(objDict.sort().values())).toEqual([1, 2, 3, 4, 5, 6, 8, 7]);
  });

  test('.sort() returns the sorted Dictionary wtesth compareFn', () => {
    expect(Array.from(objDict.sort(compareFn).values())).toEqual([7, 8, 6, 5, 4, 3, 2, 1]);
  });

  test('.sort() sort the dictionary', () => {
    objDict.sort();
    expect(Array.from(objDict.values())).toEqual([1, 2, 3, 4, 5, 6, 8, 7]);
  });

  test('.toArray() returns Array of entries', () => {
    expect(entriesDict.toArray()).toEqual(entriesData);
    expect(mapDict.toArray()).toEqual(entriesData);
    expect(objDict.toArray()).toEqual(entriesData);
    expect(dict.toArray()).toEqual(entriesData);
  });

  test('.toMap() returns Map of entries', () => {
    expect(entriesDict.toMap()).toEqual(mapData);
    expect(mapDict.toMap()).toEqual(mapData);
    expect(objDict.toMap()).toEqual(mapData);
    expect(dict.toMap()).toEqual(mapData);
  });

  test('.toObject() returns Object of entries', () => {
    expect(entriesDict.toObject()).toEqual(objData);
    expect(mapDict.toObject()).toEqual(objData);
    expect(objDict.toObject()).toEqual(objData);
    expect(dict.toObject()).toEqual(objData);
  });

  test('.values() returns array of valuesData', () => {
    expect(Array.from(emptyDict.values())).toEqual([]);
    expect(Array.from(entriesDict.values())).toEqual(valuesData);
    expect(Array.from(mapDict.values())).toEqual(valuesData);
    expect(Array.from(objDict.values())).toEqual(valuesData);
    expect(Array.from(dict.values())).toEqual(valuesData);
  });
});
