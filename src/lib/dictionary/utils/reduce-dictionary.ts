import { Dictionary } from '../dictionary';
import { DictionaryKey } from '../types/dictionary-key.type';
import { Entry } from '../types/entry.type';

/**
 * Calls the specified function for all the entries. The return value of the function is the
 * accumulated result, and is provided as an argument in the next call to the function.
 *
 * @param dictionary The dictionary to reduce.
 * @param callbackfn A function that accepts up to three arguments.
 * @param [initialValue] If is specified, it is used as the initial value to start the
 * accumulation. The first call to the callbackfn function provides this value as an argument
 * instead of an entry value.
 * @returns The value that results from the reduction.
 */
export function reduceDictionary<K, V, T>(
  dictionary: Dictionary<K, V>,
  callbackfn: (
    previousValue: T | Entry<K, V>,
    currentValue: Entry<K, V>,
    dictionary: Dictionary<K, V>
  ) => T | Entry<K, V>,
  initialValue?: T
): T | Entry<K, V> {
  if (dictionary.size === 0 && initialValue === undefined) {
    throw new TypeError('Reduce of empty dictionary with no initial value');
  }

  const tupleArray: Entry<K, V>[] = dictionary.toArray();
  const [head, ...tail]: [DictionaryKey<K>, V][] = tupleArray;
  const array: Entry<K, V>[] = initialValue !== undefined ? tupleArray : tail;
  let reduced: T | Entry<K, V> = initialValue !== undefined ? initialValue : head;

  for (const next of array) {
    reduced = callbackfn(reduced, next, dictionary);
  }

  return reduced;
}
