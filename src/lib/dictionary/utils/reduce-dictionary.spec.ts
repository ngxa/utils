import { ObjectLiteral } from '../../object/types/object-literal.interface';
import { Dictionary } from '../dictionary';
import { Entry } from '../types/entry.type';

const objData: {} = { aA: 1, aB: 2, aC: 3, aD: 4, bA: 5, bB: 6, cC: 7, dA: 8 };

export function reduceEntry(
  previous: Entry<string, number>,
  current: Entry<string, number>
): Entry<string, number> {
  return ['all', previous[1] - current[1]];
}

export function reduceNumber(previous: number, current: Entry<string, number>): number {
  return previous - current[1];
}

export function reduceToObj(
  previous: ObjectLiteral<number>,
  current: Entry<string, number>,
  dictionary: Dictionary<string, number>
): ObjectLiteral<number> {
  const key: string = current[0];

  previous[key] = dictionary.get(key) as number;

  return previous;
}

describe('reduceDictionary(dict, fn, init?)', () => {
  let emptyDict: Dictionary<string, number>;
  let singleDict: Dictionary<string, number>;
  let objDict: Dictionary<string, number>;

  beforeEach(() => {
    emptyDict = new Dictionary();
    singleDict = new Dictionary({ aA: 1 });
    objDict = new Dictionary(objData);
  });

  test('throw error if dictionary is empty without initial value', () => {
    expect(() => emptyDict.reduce(reduceEntry)).toThrow();
  });

  test('returns the initial value if set and dictionary is empty', () => {
    expect(emptyDict.reduce(reduceEntry, ['', 0])).toEqual(['', 0]);
    expect(emptyDict.reduce(reduceNumber, 0)).toEqual(0);
    expect(emptyDict.reduce(reduceToObj, {})).toEqual({});
  });

  test('returns the first value if dictionary size is 1 without initial value', () => {
    expect(singleDict.reduce(reduceEntry)).toEqual(['aA', 1]);
  });

  test('returns the accumulated value without initialValue', () => {
    expect(objDict.reduce(reduceEntry)).toEqual(['all', -34]);
  });

  it('returns the accumulated value with initialValue', () => {
    expect(singleDict.reduce(reduceEntry, ['', 0])).toEqual(['all', -1]);
    expect(objDict.reduce(reduceEntry, ['', 0])).toEqual(['all', -36]);
  });

  test('returns the accumulated with another type', () => {
    expect(objDict.reduce(reduceNumber, 0)).toEqual(-36);
    expect(objDict.reduce(reduceToObj, {})).toEqual(objData);
    expect(singleDict.reduce(reduceToObj, {})).toEqual({ aA: 1 });
  });
});
