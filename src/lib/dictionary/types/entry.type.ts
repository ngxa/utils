import { DictionaryKey } from './dictionary-key.type';

/**
 * A dictionary tuple.
 */
export type Entry<K, V> = [DictionaryKey<K>, V];
