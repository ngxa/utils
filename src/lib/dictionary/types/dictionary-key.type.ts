/**
 * A valid dictionary key type.
 */
export type DictionaryKey<K> = K | string;
