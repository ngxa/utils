import { Dictionary } from '../dictionary';
import { DictionaryKey } from './dictionary-key.type';
import { Entry } from './entry.type';

/**
 * A dictionary-like object.
 */
export type DictionaryLike<K, V> = {} | Map<DictionaryKey<K>, V> | Entry<K, V>[] | Dictionary<K, V>;
