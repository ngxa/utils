import { HttpEvent, HttpEventType } from '@angular/common/http';

import { hasProperty } from '../object/has-property';

/**
 * Check that the value is an HTTP event.
 *
 * @param value The value to test.
 * @returns True if the value is an HTTP event.
 */
export function isHttpEvent<T>(value: any): value is HttpEvent<T> {
  return typeof value === 'object' && hasProperty(value, 'type') && value.type in HttpEventType;
}
