import { testData } from '../data/test.data';
import { isVoid } from './is-void';
import { TestEntry } from './test-entry.type';

describe('isVoid(value)', () => {
  test('returns true as type guard if value is null or undefined', () => {
    testData
      .match('null_', 'undefined')
      .forEach((entry: TestEntry) => expect(isVoid(entry[1])).toBeTruthy());
  });

  test('returns false as type guard if value is not null or undefined', () => {
    testData
      .mismatch('null_')
      .mismatch('undefined')
      .forEach((entry: TestEntry) => expect(isVoid(entry[1])).toBeFalsy());
  });
});
