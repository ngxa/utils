import { HttpEventType, HttpProgressEvent } from '@angular/common/http';

import { testData } from '../data/test.data';
import { isHttpEvent } from './is-http-event';
import { TestEntry } from './test-entry.type';

const httpProgressEvent: HttpProgressEvent = {
  type: HttpEventType.DownloadProgress,
  loaded: 20,
  total: 100
};

describe('isHttpEvent', () => {
  it('returns true for HttpProgressEvent', () => {
    expect(isHttpEvent(httpProgressEvent)).toBeTruthy();
  });

  it('returns false for not HttpEvents', () => {
    testData.forEach((entry: TestEntry) => expect(isHttpEvent(entry)).toBeFalsy());
  });
});
