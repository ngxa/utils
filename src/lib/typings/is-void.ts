/**
 * Type guard that checks if value is null or undefined.
 *
 * @param value The value to check.
 * @returns `true` as type guard if value is `null` or `undefined`, else `false`.
 */
export function isVoid(value: any): value is void {
  return value === undefined || value === null;
}
