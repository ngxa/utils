import { CommonModule } from '@angular/common';

import { TestBedConfig } from './testbed-config.class';

describe('TestBedConfig', () => {
  let testBedConfig: TestBedConfig;

  beforeEach(() => {
    testBedConfig = new TestBedConfig();
  });

  test('.providers has ComponentFixtureAutoDetect', () => {
    expect(testBedConfig.providers).toEqual([]);
  });

  test('.declarations has an empty array', () => {
    expect(testBedConfig.declarations).toEqual([]);
  });

  test('.imports has CommonModule', () => {
    expect(testBedConfig.imports).toEqual([CommonModule]);
  });

  test('.schemas has an empty array', () => {
    expect(testBedConfig.schemas).toEqual([]);
  });

  test('.entryComponents has an empty array', () => {
    expect(testBedConfig.providers).toEqual([]);
  });
});
