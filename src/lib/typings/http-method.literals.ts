/**
 * HTTP request methods to indicate the desired action to be performed for a given resource.
 */
export type HttpMethodLiterals =
  | 'GET'
  | 'HEAD'
  | 'POST'
  | 'PUT'
  | 'PATCH'
  | 'DELETE'
  | 'JSONP'
  | 'OPTIONS';
