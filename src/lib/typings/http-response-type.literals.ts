/**
 * The expected response type of the server.
 */
export type HttpResponseTypeLiterals = 'arraybuffer' | 'blob' | 'json' | 'text';
