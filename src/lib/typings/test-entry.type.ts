import { Entry } from '../dictionary/types/entry.type';

/**
 * Entry type for tests data.
 */
export type TestEntry = Entry<string, any>;
