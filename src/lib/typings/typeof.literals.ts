/**
 * Basic types as string literals.
 */
export type TypeofLiterals =
  | 'undefined'
  | 'object'
  | 'boolean'
  | 'number'
  | 'string'
  | 'function'
  | 'symbol';
