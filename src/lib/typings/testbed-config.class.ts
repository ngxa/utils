import { CommonModule } from '@angular/common';
import { SchemaMetadata } from '@angular/core';
import { TestModuleMetadata } from '@angular/core/testing';

/**
 * Basic configuration fot TestBed.
 *
 * Any dependency should be added to this object when the test is created.
 */
export class TestBedConfig implements TestModuleMetadata {
  /**
   * Services to provide for the tests.
   */
  providers: any[] = [];
  /**
   * Components and directives to add to the test.
   */
  declarations: any[] = [];
  /**
   * Modules requireds by the test.
   */
  imports: any[] = [CommonModule];
  /**
   * Schemas requireds by the test.
   */
  schemas: (SchemaMetadata | any[])[] = [];
  /**
   * AOT summaries required by the test.
   */
  aotSummaries?: (() => any[]);
  /**
   * Entry components required by the test.
   */
  entryComponents: any[] = [];
}
