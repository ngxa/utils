import { testData } from './data/test.data';
import { defaults } from './defaults';
import { TestEntry } from './typings/test-entry.type';

describe('defaults(value, orValue)', () => {
  it('returns value if is not undefined', () => {
    testData
      .mismatch('undefined')
      .mismatch('null_')
      .forEach((entry: TestEntry) => expect(defaults(entry[1], 'anotherValue')).toEqual(entry[1]));
  });

  it('returns orValue if value is undefined', () => {
    expect(defaults(undefined, 'anotherValue')).toEqual('anotherValue');
  });

  it('returns orValue if value is null', () => {
    expect(defaults(null, 'anotherValue')).toEqual('anotherValue');
  });
});
