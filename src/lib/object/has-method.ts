import { hasMember } from './has-member';
import { hasProperty } from './has-property';

/**
 * Check if key is a defined object method.
 *
 * @param obj The object that contains the method.
 * @param key The key of the method.
 * @returns `true` if the key is a method of the object, if not `false`.
 */
export function hasMethod(obj: any, key: PropertyKey): boolean {
  return typeof obj === 'object' && (hasMember(obj, key) && !hasProperty(obj, key));
}
