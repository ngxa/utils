import { cleanObject } from './clean-object';

describe('cleanObject(obj)', () => {
  test('returns the same object if no empties', () => {
    expect(cleanObject({ aAA: 1, aAB: 2 })).toEqual({ aAA: 1, aAB: 2 });
  });

  test('returns the object without null values', () => {
    expect(cleanObject({ aAA: 1, aAB: null })).toEqual({ aAA: 1 });
  });

  test('returns the object without undefined values', () => {
    expect(cleanObject({ aAA: 1, aAB: undefined })).toEqual({ aAA: 1 });
  });

  test('returns the object without empty string values', () => {
    expect(cleanObject({ aAA: 1, aAB: '' })).toEqual({ aAA: 1 });
  });

  test('returns the object without multiple empty string values', () => {
    expect(cleanObject({ aAA: 1, aAB: '  ' })).toEqual({ aAA: 1 });
  });
});
