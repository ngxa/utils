import { GrandTestClass } from '../data/grand-test-class.data';
import { ParentTestClass } from '../data/parent-test-class.data';
import { TestClass } from '../data/test-class.data';
import { objectInheritsFrom } from './object-inherits-from';

describe('objectInheritsFrom(obj, clazz)', () => {
  it('.objectInheritsFrom(obj, clazz) test a class inherith directly', () => {
    objectInheritsFrom(new TestClass(), ParentTestClass);
  });

  it('.objectInheritsFrom(obj, clazz) test a class inherith indirectly', () => {
    objectInheritsFrom(new TestClass(), GrandTestClass);
  });

  it('.objectInheritsFrom(parent) test a class do not inherith', () => {
    objectInheritsFrom(new GrandTestClass(), TestClass);
  });
});
