import { getPropertyDescriptor } from './get-property-descriptor';
import { hasMember } from './has-member';

/**
 * Check if key is a defined setter.
 *
 * @param obj The object that contains the setter.
 * @param key The key of the setter property.
 * @returns `true` if the property is a defined setter, if not `false`.
 */
export function hasSetter(obj: any, key: PropertyKey): boolean {
  if (typeof obj !== 'object') {
    return false;
  }

  const descriptor: PropertyDescriptor | undefined = getPropertyDescriptor(obj, key);

  return descriptor === undefined ? false : hasDefinedSetter(descriptor);
}

/**
 * @internal
 * @param descriptor The property descriptor.
 * @returns `true` if `descriptor` has a defined setter, if not `false`.
 */
function hasDefinedSetter(descriptor: PropertyDescriptor): boolean {
  return hasMember(descriptor, 'set') && descriptor.set !== undefined;
}
