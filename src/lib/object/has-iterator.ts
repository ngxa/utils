import { isVoid } from '../typings/is-void';

/**
 * Check that the value an object with an iterator method.
 *
 * @param value The value to check.
 * @returns `true` if the value is an object with an iterator method, if not `false`.
 */
export function hasIterator<T>(value: any): value is Iterator<T> {
  return !isVoid(value) && typeof value === 'object' && Symbol.iterator in value;
}
