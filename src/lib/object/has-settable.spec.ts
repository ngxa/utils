import { TestClass } from '../data/test-class.data';

import { hasSettable } from './has-settable';

describe('hasSettable(obj, key)', () => {
  let obj: TestClass;

  beforeEach(() => {
    obj = new TestClass();
  });

  test('returns false for non object', () => {
    expect(hasSettable('', 'prop')).toEqual(false);
  });

  test('returns false for non existant properties', () => {
    expect(hasSettable(obj, 'nonExistantProp')).toEqual(false);
  });

  test('returns false for undefined own properties', () => {
    expect(hasSettable(obj, 'undefinedProp')).toEqual(false);
  });

  test('returns false for undefined properties in prototype chain', () => {
    expect(hasSettable(obj, 'undefinedParentProp')).toEqual(false);
    expect(hasSettable(obj, 'undefinedGrandProp')).toEqual(false);
  });

  test('returns true for own properties', () => {
    expect(hasSettable(obj, 'prop')).toEqual(true);
  });

  test('returns true for properties in prototype chain', () => {
    expect(hasSettable(obj, 'parentProp')).toEqual(true);
    expect(hasSettable(obj, 'grandProp')).toEqual(true);
  });

  test('returns true for own getters/setters', () => {
    expect(hasSettable(obj, 'getterSetterProp')).toEqual(true);
  });

  test('returns true for getters/setters in prototype chain', () => {
    expect(hasSettable(obj, 'getterSetterParentProp')).toEqual(true);
    expect(hasSettable(obj, 'getterSetterGrandProp')).toEqual(true);
  });

  test('returns false for own getters', () => {
    expect(hasSettable(obj, 'getterProp')).toEqual(false);
  });

  test('returns false for getters in prototype chain', () => {
    expect(hasSettable(obj, 'getterParentProp')).toEqual(false);
    expect(hasSettable(obj, 'getterGrandProp')).toEqual(false);
  });

  test('returns true for own setters', () => {
    expect(hasSettable(obj, 'setterProp')).toEqual(true);
  });

  test('returns true for setters in prototype chain', () => {
    expect(hasSettable(obj, 'setterParentProp')).toEqual(true);
    expect(hasSettable(obj, 'setterGrandProp')).toEqual(true);
  });

  test('returns false for own methods', () => {
    expect(hasSettable(obj, 'method')).toEqual(false);
  });

  test('returns false for methods in prototype chain', () => {
    expect(hasSettable(obj, 'parentMethod')).toEqual(false);
    expect(hasSettable(obj, 'grandMethod')).toEqual(false);
  });
});
