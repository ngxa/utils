import { isVoid } from '../typings/is-void';
import { ObjectLiteral } from './types/object-literal.interface';

/**
 * Returns the object without empty values.
 *
 * All keys with `null`, `undefined` or empty string `''` values are removed from the object.
 * @param obj The object to check.
 * @returns the object without empty values.
 */
export function cleanObject(obj: ObjectLiteral): ObjectLiteral {
  const newObj: ObjectLiteral = Object.create(null);

  Object.keys(obj).forEach((key: string) => {
    if (!isVoid(obj[key]) && obj[key].toString().trim() !== '') {
      newObj[key] = obj[key];
    }
  });

  return newObj;
}
