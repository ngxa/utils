import { TestClass } from '../data/test-class.data';

import { hasGetterSetter } from './has-getter-setter';

describe('hasGetterSetter(obj, key)', () => {
  let obj: TestClass;

  beforeEach(() => {
    obj = new TestClass();
  });

  test('returns false for non object', () => {
    expect(hasGetterSetter('', 'prop')).toEqual(false);
  });

  test('returns false for non existant properties', () => {
    expect(hasGetterSetter(obj, 'nonExistantProp')).toEqual(false);
  });

  test('returns false for undefined own properties', () => {
    expect(hasGetterSetter(obj, 'undefinedProp')).toEqual(false);
  });

  test('returns false for undefined properties in prototype chain', () => {
    expect(hasGetterSetter(obj, 'undefinedParentProp')).toEqual(false);
    expect(hasGetterSetter(obj, 'undefinedGrandProp')).toEqual(false);
  });

  test('returns false for own properties', () => {
    expect(hasGetterSetter(obj, 'prop')).toEqual(false);
  });

  test('returns false for properties in prototype chain', () => {
    expect(hasGetterSetter(obj, 'parentProp')).toEqual(false);
    expect(hasGetterSetter(obj, 'grandProp')).toEqual(false);
  });

  test('returns true for own getters/setters', () => {
    expect(hasGetterSetter(obj, 'getterSetterProp')).toEqual(true);
  });

  test('returns true for getters/setters in prototype chain', () => {
    expect(hasGetterSetter(obj, 'getterSetterParentProp')).toEqual(true);
    expect(hasGetterSetter(obj, 'getterSetterGrandProp')).toEqual(true);
  });

  test('returns true for own getters', () => {
    expect(hasGetterSetter(obj, 'getterProp')).toEqual(true);
  });

  test('returns true for getters in prototype chain', () => {
    expect(hasGetterSetter(obj, 'getterParentProp')).toEqual(true);
    expect(hasGetterSetter(obj, 'getterGrandProp')).toEqual(true);
  });

  test('returns true for own setters', () => {
    expect(hasGetterSetter(obj, 'setterProp')).toEqual(true);
  });

  test('returns true for setters in prototype chain', () => {
    expect(hasGetterSetter(obj, 'setterParentProp')).toEqual(true);
    expect(hasGetterSetter(obj, 'setterGrandProp')).toEqual(true);
  });

  test('returns false for own methods', () => {
    expect(hasGetterSetter(obj, 'method')).toEqual(false);
  });

  test('returns false for methods in prototype chain', () => {
    expect(hasGetterSetter(obj, 'parentMethod')).toEqual(false);
    expect(hasGetterSetter(obj, 'grandMethod')).toEqual(false);
  });
});
