import { filterObjectKeys } from './filter-object-keys';

const fromObj: {} = { aAA: 1, aAB: 2, aC: 3, aD: 4, bA: 5, bB: 6, _cC: 7, dAD: 8 };

describe('filterObjectKeys(obj, ...keys)', () => {
  test('returns empty object from empty object', () => {
    expect(filterObjectKeys({}, 'aAA')).toEqual({});
  });

  test('returns empty object if no keys', () => {
    expect(filterObjectKeys(fromObj)).toEqual({});
  });

  test('returns object filtered with single key', () => {
    expect(filterObjectKeys(fromObj, 'aAA')).toEqual({ aAA: 1 });
  });

  test('returns object filtered with multiple keys', () => {
    expect(filterObjectKeys(fromObj, 'aAA', 'aAB')).toEqual({ aAA: 1, aAB: 2 });
    expect(filterObjectKeys(fromObj, 'aAA', 'aAB', 'aC')).toEqual({ aAA: 1, aAB: 2, aC: 3 });
  });

  test('returns filtered object avoiding non existant keys', () => {
    expect(filterObjectKeys(fromObj, 'aAA', 'nonExistant')).toEqual({ aAA: 1 });
  });
});
