import { hasGetterSetter } from './has-getter-setter';

/**
 * Check if key is a defined object property, including getters and setters.
 *
 * @param obj The object that contains the property.
 * @param key The key of the property.
 * @returns `true` if the key is a property of the object, if not `false`.
 */
export function hasProperty(obj: any, key: PropertyKey): boolean {
  return typeof obj === 'object' && (obj.hasOwnProperty(key) || hasGetterSetter(obj, key));
}
