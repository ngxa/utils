import { TestClass } from '../data/test-class.data';

import { hasSetter } from './has-setter';

describe('hasSetter(obj, key)', () => {
  let obj: TestClass;

  beforeEach(() => {
    obj = new TestClass();
  });

  test('returns false for non object', () => {
    expect(hasSetter('', 'prop')).toEqual(false);
  });

  test('returns false for non existant properties', () => {
    expect(hasSetter(obj, 'nonExistantProp')).toEqual(false);
  });

  test('returns false for undefined own properties', () => {
    expect(hasSetter(obj, 'undefinedProp')).toEqual(false);
  });

  test('returns false for undefined properties in prototype chain', () => {
    expect(hasSetter(obj, 'undefinedParentProp')).toEqual(false);
    expect(hasSetter(obj, 'undefinedGrandProp')).toEqual(false);
  });

  test('returns false for own properties', () => {
    expect(hasSetter(obj, 'prop')).toEqual(false);
  });

  test('returns false for properties in prototype chain', () => {
    expect(hasSetter(obj, 'parentProp')).toEqual(false);
    expect(hasSetter(obj, 'grandProp')).toEqual(false);
  });

  test('returns true for own getters/setters', () => {
    expect(hasSetter(obj, 'getterSetterProp')).toEqual(true);
  });

  test('returns true for getters/setters in prototype chain', () => {
    expect(hasSetter(obj, 'getterSetterParentProp')).toEqual(true);
    expect(hasSetter(obj, 'getterSetterGrandProp')).toEqual(true);
  });

  test('returns false for own getters', () => {
    expect(hasSetter(obj, 'getterProp')).toEqual(false);
  });

  test('returns false for getters in prototype chain', () => {
    expect(hasSetter(obj, 'getterParentProp')).toEqual(false);
    expect(hasSetter(obj, 'getterGrandProp')).toEqual(false);
  });

  test('returns true for own setters', () => {
    expect(hasSetter(obj, 'setterProp')).toEqual(true);
  });

  test('returns true for setters in prototype chain', () => {
    expect(hasSetter(obj, 'setterParentProp')).toEqual(true);
    expect(hasSetter(obj, 'setterGrandProp')).toEqual(true);
  });

  test('returns false for own methods', () => {
    expect(hasSetter(obj, 'method')).toEqual(false);
  });

  test('returns false for methods in prototype chain', () => {
    expect(hasSetter(obj, 'parentMethod')).toEqual(false);
    expect(hasSetter(obj, 'grandMethod')).toEqual(false);
  });
});
