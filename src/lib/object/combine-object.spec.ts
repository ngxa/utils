import { combineObject } from './combine-object';

const aa: {} = { aa: 1 };
const bb: {} = { bb: '1' };

describe('combineObject(...objs)', () => {
  test('returns {} if no args', () => {
    expect(combineObject()).toEqual({});
  });

  test('returns {} if empty object', () => {
    expect(combineObject({})).toEqual({});
  });

  test('returns single object members', () => {
    expect(combineObject(aa)).toEqual(aa);
  });

  test('returns combined multiple objects members', () => {
    expect(combineObject(aa, bb)).toEqual({ aa: 1, bb: '1' });
  });

  test('returns last repeated member', () => {
    expect(combineObject(aa, bb, { aa: 0 })).toEqual({ aa: 0, bb: '1' });
  });
});
