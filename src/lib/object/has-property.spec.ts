import { TestClass } from '../data/test-class.data';

import { hasProperty } from './has-property';

describe('hasProperty(obj, key)', () => {
  let obj: TestClass;

  beforeEach(() => {
    obj = new TestClass();
  });

  test('returns false for non object', () => {
    expect(hasProperty('', 'prop')).toEqual(false);
  });

  test('returns false for non existant properties', () => {
    expect(hasProperty(obj, 'nonExistantProp')).toEqual(false);
  });

  test('returns false for undefined own properties', () => {
    expect(hasProperty(obj, 'undefinedProp')).toEqual(false);
  });

  test('returns false for undefined properties in prototype chain', () => {
    expect(hasProperty(obj, 'undefinedParentProp')).toEqual(false);
    expect(hasProperty(obj, 'undefinedGrandProp')).toEqual(false);
  });

  test('returns true for own properties', () => {
    expect(hasProperty(obj, 'prop')).toEqual(true);
  });

  test('returns true for properties in prototype chain', () => {
    expect(hasProperty(obj, 'parentProp')).toEqual(true);
    expect(hasProperty(obj, 'grandProp')).toEqual(true);
  });

  test('returns true for own getters/setters', () => {
    expect(hasProperty(obj, 'getterSetterProp')).toEqual(true);
  });

  test('returns true for getters/setters in prototype chain', () => {
    expect(hasProperty(obj, 'getterSetterParentProp')).toEqual(true);
    expect(hasProperty(obj, 'getterSetterGrandProp')).toEqual(true);
  });

  test('returns true for own getters', () => {
    expect(hasProperty(obj, 'getterProp')).toEqual(true);
  });

  test('returns true for getters in prototype chain', () => {
    expect(hasProperty(obj, 'getterParentProp')).toEqual(true);
    expect(hasProperty(obj, 'getterGrandProp')).toEqual(true);
  });

  test('returns true for own setters', () => {
    expect(hasProperty(obj, 'setterProp')).toEqual(true);
  });

  test('returns true for setters in prototype chain', () => {
    expect(hasProperty(obj, 'setterParentProp')).toEqual(true);
    expect(hasProperty(obj, 'setterGrandProp')).toEqual(true);
  });

  test('returns false for own methods', () => {
    expect(hasProperty(obj, 'method')).toEqual(false);
  });

  test('returns false for methods in prototype chain', () => {
    expect(hasProperty(obj, 'parentMethod')).toEqual(false);
    expect(hasProperty(obj, 'grandMethod')).toEqual(false);
  });
});
