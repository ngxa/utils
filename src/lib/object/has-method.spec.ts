import { TestClass } from '../data/test-class.data';

import { hasMethod } from './has-method';

describe('hasMethod(obj, key)', () => {
  let obj: TestClass;

  beforeEach(() => {
    obj = new TestClass();
  });

  test('returns false for non object', () => {
    expect(hasMethod('', 'prop')).toEqual(false);
  });

  test('returns false for non existant properties', () => {
    expect(hasMethod(obj, 'nonExistantProp')).toEqual(false);
  });

  test('returns false for undefined own properties', () => {
    expect(hasMethod(obj, 'undefinedProp')).toEqual(false);
  });

  test('returns false for undefined properties in prototype chain', () => {
    expect(hasMethod(obj, 'undefinedParentProp')).toEqual(false);
    expect(hasMethod(obj, 'undefinedGrandProp')).toEqual(false);
  });

  test('returns false for own properties', () => {
    expect(hasMethod(obj, 'prop')).toEqual(false);
  });

  test('returns false for properties in prototype chain', () => {
    expect(hasMethod(obj, 'parentProp')).toEqual(false);
    expect(hasMethod(obj, 'grandProp')).toEqual(false);
  });

  test('returns false for own getters/setters', () => {
    expect(hasMethod(obj, 'getterSetterProp')).toEqual(false);
  });

  test('returns false for getters/setters in prototype chain', () => {
    expect(hasMethod(obj, 'getterSetterParentProp')).toEqual(false);
    expect(hasMethod(obj, 'getterSetterGrandProp')).toEqual(false);
  });

  test('returns false for own getters', () => {
    expect(hasMethod(obj, 'getterProp')).toEqual(false);
  });

  test('returns false for getters in prototype chain', () => {
    expect(hasMethod(obj, 'getterParentProp')).toEqual(false);
    expect(hasMethod(obj, 'getterGrandProp')).toEqual(false);
  });

  test('returns false for own setters', () => {
    expect(hasMethod(obj, 'setterProp')).toEqual(false);
  });

  test('returns false for setters in prototype chain', () => {
    expect(hasMethod(obj, 'setterParentProp')).toEqual(false);
    expect(hasMethod(obj, 'setterGrandProp')).toEqual(false);
  });

  test('returns true for own methods', () => {
    expect(hasMethod(obj, 'method')).toEqual(true);
  });

  test('returns true for methods in prototype chain', () => {
    expect(hasMethod(obj, 'parentMethod')).toEqual(true);
    expect(hasMethod(obj, 'grandMethod')).toEqual(true);
  });
});
