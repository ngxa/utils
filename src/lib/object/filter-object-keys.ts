import { ObjectKey } from './types/object-key.type';
import { ObjectLiteral } from './types/object-literal.interface';

/**
 * Get a new object with the specified keys and its values.
 *
 * @param obj Original object.
 * @param keys Keys to include in the new object.
 * @returns The object with the specified keys and its values.
 */
export function filterObjectKeys<T extends ObjectLiteral>(obj: T, ...keys: ObjectKey[]): T {
  return keys.reduce((newObj: T, key: ObjectKey) => {
    newObj[key] = obj[key];

    return newObj;
  }, Object.create(null));
}
