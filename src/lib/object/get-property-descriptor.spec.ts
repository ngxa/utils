import { GrandTestClass } from '../data/grand-test-class.data';
import { ParentTestClass } from '../data/parent-test-class.data';
import { TestClass } from '../data/test-class.data';

import { getPropertyDescriptor } from './get-property-descriptor';

describe('getPropertyDescriptor(obj, key)', () => {
  let obj: TestClass;

  beforeEach(() => {
    obj = new TestClass();
  });

  test('returns undefined for non object', () => {
    expect(getPropertyDescriptor('', 'prop')).toBeUndefined();
  });

  test('returns undefined for non existant properties', () => {
    expect(getPropertyDescriptor(obj, 'nonExistantProp')).toBeUndefined();
  });

  test('returns undefined for undefined own properties', () => {
    expect(getPropertyDescriptor(obj, 'undefinedProp')).toBeUndefined();
  });

  test('returns undefined for undefined properties in prototype chain', () => {
    expect(getPropertyDescriptor(obj, 'undefinedParentProp')).toBeUndefined();
    expect(getPropertyDescriptor(obj, 'undefinedGrandProp')).toBeUndefined();
  });

  test('returns own properties descriptor', () => {
    const prop: PropertyDescriptor | undefined = getPropertyDescriptor(obj, 'prop');

    expect(prop).toBeDefined();
    expect(prop).toEqual(Object.getOwnPropertyDescriptor(obj, 'prop'));
  });

  test('returns properties descriptor in prototype chain', () => {
    const parentProp: PropertyDescriptor | undefined = getPropertyDescriptor(obj, 'parentProp');
    const grandProp: PropertyDescriptor | undefined = getPropertyDescriptor(obj, 'grandProp');

    expect(parentProp).toBeDefined();
    expect(grandProp).toBeDefined();
    expect(parentProp).toEqual(Object.getOwnPropertyDescriptor(obj, 'parentProp'));
    expect(grandProp).toEqual(Object.getOwnPropertyDescriptor(obj, 'grandProp'));
  });

  test('returns own getters/setters descriptor', () => {
    const getterSetterProp: PropertyDescriptor | undefined = getPropertyDescriptor(
      obj,
      'getterSetterProp'
    );

    expect(getterSetterProp).toBeDefined();
    expect(getterSetterProp).toEqual(
      Object.getOwnPropertyDescriptor(TestClass.prototype, 'getterSetterProp')
    );
  });

  test('returns getters/setters descriptor in prototype chain', () => {
    const getterSetterParentProp: PropertyDescriptor | undefined = getPropertyDescriptor(
      obj,
      'getterSetterParentProp'
    );
    const getterSetterGrandProp: PropertyDescriptor | undefined = getPropertyDescriptor(
      obj,
      'getterSetterGrandProp'
    );

    expect(getterSetterParentProp).toBeDefined();
    expect(getterSetterGrandProp).toBeDefined();
    expect(getterSetterParentProp).toEqual(
      Object.getOwnPropertyDescriptor(ParentTestClass.prototype, 'getterSetterParentProp')
    );
    expect(getterSetterGrandProp).toEqual(
      Object.getOwnPropertyDescriptor(GrandTestClass.prototype, 'getterSetterGrandProp')
    );
  });

  test('returns own getters descriptor', () => {
    const getterProp: PropertyDescriptor | undefined = getPropertyDescriptor(obj, 'getterProp');

    expect(getterProp).toBeDefined();
    expect(getterProp).toEqual(Object.getOwnPropertyDescriptor(TestClass.prototype, 'getterProp'));
  });

  test('returns getters descriptor in prototype chain', () => {
    const getterParentProp: PropertyDescriptor | undefined = getPropertyDescriptor(
      obj,
      'getterParentProp'
    );
    const getterGrandProp: PropertyDescriptor | undefined = getPropertyDescriptor(
      obj,
      'getterGrandProp'
    );

    expect(getterParentProp).toBeDefined();
    expect(getterGrandProp).toBeDefined();
    expect(getterParentProp).toEqual(
      Object.getOwnPropertyDescriptor(ParentTestClass.prototype, 'getterParentProp')
    );
    expect(getterGrandProp).toEqual(
      Object.getOwnPropertyDescriptor(GrandTestClass.prototype, 'getterGrandProp')
    );
  });

  test('returns own setters descriptor', () => {
    const setterProp: PropertyDescriptor | undefined = getPropertyDescriptor(obj, 'setterProp');

    expect(setterProp).toBeDefined();
    expect(setterProp).toEqual(Object.getOwnPropertyDescriptor(TestClass.prototype, 'setterProp'));
  });

  test('returns setters descriptor in prototype chain', () => {
    const setterParentProp: PropertyDescriptor | undefined = getPropertyDescriptor(
      obj,
      'setterParentProp'
    );
    const setterGrandProp: PropertyDescriptor | undefined = getPropertyDescriptor(
      obj,
      'setterGrandProp'
    );

    expect(setterParentProp).toBeDefined();
    expect(setterGrandProp).toBeDefined();
    expect(setterParentProp).toEqual(
      Object.getOwnPropertyDescriptor(ParentTestClass.prototype, 'setterParentProp')
    );
    expect(setterGrandProp).toEqual(
      Object.getOwnPropertyDescriptor(GrandTestClass.prototype, 'setterGrandProp')
    );
  });

  test('returns own methods descriptor', () => {
    const method: PropertyDescriptor | undefined = getPropertyDescriptor(obj, 'method');

    expect(method).toBeDefined();
    expect(method).toEqual(Object.getOwnPropertyDescriptor(TestClass.prototype, 'method'));
  });

  test('returns methods descriptor in prototype chain', () => {
    const parentMethod: PropertyDescriptor | undefined = getPropertyDescriptor(obj, 'parentMethod');
    const grandMethod: PropertyDescriptor | undefined = getPropertyDescriptor(obj, 'grandMethod');

    expect(parentMethod).toBeDefined();
    expect(grandMethod).toBeDefined();
    expect(parentMethod).toEqual(
      Object.getOwnPropertyDescriptor(ParentTestClass.prototype, 'parentMethod')
    );
    expect(grandMethod).toEqual(
      Object.getOwnPropertyDescriptor(GrandTestClass.prototype, 'grandMethod')
    );
  });
});
