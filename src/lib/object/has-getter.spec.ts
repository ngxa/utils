import { TestClass } from '../data/test-class.data';

import { hasGetter } from './has-getter';

describe('hasGetter(obj, key)', () => {
  let obj: TestClass;

  beforeEach(() => {
    obj = new TestClass();
  });

  test('returns false for non object', () => {
    expect(hasGetter('', 'prop')).toEqual(false);
  });

  test('returns false for non existant properties', () => {
    expect(hasGetter(obj, 'nonExistantProp')).toEqual(false);
  });

  test('returns false for undefined own properties', () => {
    expect(hasGetter(obj, 'undefinedProp')).toEqual(false);
  });

  test('returns false for undefined properties in prototype chain', () => {
    expect(hasGetter(obj, 'undefinedParentProp')).toEqual(false);
    expect(hasGetter(obj, 'undefinedGrandProp')).toEqual(false);
  });

  test('returns false for own properties', () => {
    expect(hasGetter(obj, 'prop')).toEqual(false);
  });

  test('returns false for properties in prototype chain', () => {
    expect(hasGetter(obj, 'parentProp')).toEqual(false);
    expect(hasGetter(obj, 'grandProp')).toEqual(false);
  });

  test('returns true for own getters/setters', () => {
    expect(hasGetter(obj, 'getterSetterProp')).toEqual(true);
  });

  test('returns true for getters/setters in prototype chain', () => {
    expect(hasGetter(obj, 'getterSetterParentProp')).toEqual(true);
    expect(hasGetter(obj, 'getterSetterGrandProp')).toEqual(true);
  });

  test('returns true for own getters', () => {
    expect(hasGetter(obj, 'getterProp')).toEqual(true);
  });

  test('returns true for getters in prototype chain', () => {
    expect(hasGetter(obj, 'getterParentProp')).toEqual(true);
    expect(hasGetter(obj, 'getterGrandProp')).toEqual(true);
  });

  test('returns false for own setters', () => {
    expect(hasGetter(obj, 'setterProp')).toEqual(false);
  });

  test('returns false for setters in prototype chain', () => {
    expect(hasGetter(obj, 'setterParentProp')).toEqual(false);
    expect(hasGetter(obj, 'setterGrandProp')).toEqual(false);
  });

  test('returns false for own methods', () => {
    expect(hasGetter(obj, 'method')).toEqual(false);
  });

  test('returns false for methods in prototype chain', () => {
    expect(hasGetter(obj, 'parentMethod')).toEqual(false);
    expect(hasGetter(obj, 'grandMethod')).toEqual(false);
  });
});
