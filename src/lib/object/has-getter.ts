import { getPropertyDescriptor } from './get-property-descriptor';
import { hasMember } from './has-member';

/**
 * Check if key is a defined getter.
 *
 * @param obj The object that contains the getter.
 * @param key The key of the getter.
 * @returns `true` if the key is a defined getter, if not `false`.
 */
export function hasGetter(obj: any, key: PropertyKey): boolean {
  if (typeof obj !== 'object') {
    return false;
  }

  const descriptor: PropertyDescriptor | undefined = getPropertyDescriptor(obj, key);

  return descriptor === undefined ? false : hasDefinedGetter(descriptor);
}

/**
 * @internal
 * @param descriptor Property descriptor.
 * @returns `true` if `descriptor` has a defined getter, otherwise `false`.
 */
function hasDefinedGetter(descriptor: PropertyDescriptor): boolean {
  return hasMember(descriptor, 'get') && descriptor.get !== undefined;
}
