import { TestClass } from '../data/test-class.data';

import { hasGettable } from './has-gettable';

describe('hasGettable(obj, key)', () => {
  let obj: TestClass;

  beforeEach(() => {
    obj = new TestClass();
  });

  test('returns false for non object', () => {
    expect(hasGettable('', 'prop')).toEqual(false);
  });

  test('returns false for non existant properties', () => {
    expect(hasGettable(obj, 'nonExistantProp')).toEqual(false);
  });

  test('returns false for undefined own properties', () => {
    expect(hasGettable(obj, 'undefinedProp')).toEqual(false);
  });

  test('returns false for undefined properties in prototype chain', () => {
    expect(hasGettable(obj, 'undefinedParentProp')).toEqual(false);
    expect(hasGettable(obj, 'undefinedGrandProp')).toEqual(false);
  });

  test('returns true for own properties', () => {
    expect(hasGettable(obj, 'prop')).toEqual(true);
  });

  test('returns true for properties in prototype chain', () => {
    expect(hasGettable(obj, 'parentProp')).toEqual(true);
    expect(hasGettable(obj, 'grandProp')).toEqual(true);
  });

  test('returns true for own getters/setters', () => {
    expect(hasGettable(obj, 'getterSetterProp')).toEqual(true);
  });

  test('returns true for getters/setters in prototype chain', () => {
    expect(hasGettable(obj, 'getterSetterParentProp')).toEqual(true);
    expect(hasGettable(obj, 'getterSetterGrandProp')).toEqual(true);
  });

  test('returns true for own getters', () => {
    expect(hasGettable(obj, 'getterProp')).toEqual(true);
  });

  test('returns true for getters in prototype chain', () => {
    expect(hasGettable(obj, 'getterParentProp')).toEqual(true);
    expect(hasGettable(obj, 'getterGrandProp')).toEqual(true);
  });

  test('returns false for own setters', () => {
    expect(hasGettable(obj, 'setterProp')).toEqual(false);
  });

  test('returns false for setters in prototype chain', () => {
    expect(hasGettable(obj, 'setterParentProp')).toEqual(false);
    expect(hasGettable(obj, 'setterGrandProp')).toEqual(false);
  });

  test('returns false for own methods', () => {
    expect(hasGettable(obj, 'method')).toEqual(false);
  });

  test('returns false for methods in prototype chain', () => {
    expect(hasGettable(obj, 'parentMethod')).toEqual(false);
    expect(hasGettable(obj, 'grandMethod')).toEqual(false);
  });
});
