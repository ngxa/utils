import { ObjectLiteral } from './types/object-literal.interface';

/**
 * Combine multiple objects into one.
 *
 * Returns an empty object if no arguments. If a key is repeated, the last value is taken.
 * @param objs The objects to combine.
 * @returns The combined object.
 */
export function combineObject(...objs: ObjectLiteral[]): {} {
  const newObj: ObjectLiteral = Object.create(null);

  objs.forEach((obj: ObjectLiteral) => {
    Object.keys(obj).forEach((key: string) => (newObj[key] = obj[key]));
  });

  return newObj;
}
