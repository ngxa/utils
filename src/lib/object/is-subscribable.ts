import { Subscribable } from 'rxjs';

import { isVoid } from '../typings/is-void';

/**
 * Type guard that check if an object implements `Subscribable`.
 *
 * @param value The value to check.
 * @returns `true` if is an object and implements `Subscribable`, else `false`.
 */
export function isSubscribable<T>(value: any): value is Subscribable<T> {
  return (
    !isVoid(value) &&
    typeof value === 'object' &&
    'subscribe' in value &&
    typeof value.subscribe === 'function'
  );
}
