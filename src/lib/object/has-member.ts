/**
 * Check if key is a defined object member.
 *
 * @param obj The object that contains the member.
 * @param key The key of the member.
 * @returns `true` if the key is a member of the object, if not `false`.
 */
export function hasMember(obj: any, key: PropertyKey): boolean {
  return typeof obj === 'object' && key in obj;
}
