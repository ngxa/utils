/**
 * Valid object key type.
 */
export type ObjectKey = number | string;
