/**
 * An object literal with values of the specified type.
 */
export interface ObjectLiteral<T = any> {
  [key: string]: T;
  [key: number]: T;
}
