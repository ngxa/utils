import { testData } from '../data/test.data';
import { TestEntry } from '../typings/test-entry.type';
import { isSubscribable } from './is-subscribable';

describe('isSubscribable(value)', () => {
  test('returns true as type guard if value is Subscribable', () => {
    testData
      .match('Subscribable')
      .forEach((entry: TestEntry) => expect(isSubscribable(entry[1])).toBeTruthy());
  });

  test('returns false as type guard if value is not Subscribable', () => {
    testData
      .mismatch('Subscribable')
      .forEach((entry: TestEntry) => expect(isSubscribable(entry[1])).toBeFalsy());
  });
});
