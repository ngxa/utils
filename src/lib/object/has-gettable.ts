import { hasGetter } from './has-getter';

/**
 * Check if key is a gettable property.
 *
 * @export
 * @param obj The object that contains the property or getter.
 * @param key The key of the property or getter.
 * @returns `true` if the key is a defined gettable property, if not `false`.
 */
export function hasGettable(obj: any, key: PropertyKey): boolean {
  return typeof obj === 'object' && (obj.hasOwnProperty(key) || hasGetter(obj, key));
}
