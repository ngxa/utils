import { objectEntries } from './object-entries';

const objString: {} = { foo: 'bar', baz: 42 };
const objNumber: {} = { 100: 'a', 2: 'b', 7: 'c' };
const arrayString: [string, string | number][] = [['foo', 'bar'], ['baz', 42]];
const arrayNumber: [string, string][] = [['2', 'b'], ['7', 'c'], ['100', 'a']];

describe('objectEntries(obj)', () => {
  test('returns empty array from empty object', () => {
    expect(objectEntries({})).toEqual([]);
  });

  test('returns array from object with atring key', () => {
    expect(objectEntries(objString)).toEqual(arrayString);
  });

  test('returns array from object with number key', () => {
    expect(objectEntries(objNumber)).toEqual(arrayNumber);
  });
});
