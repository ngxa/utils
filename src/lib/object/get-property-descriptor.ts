import { isVoid } from '../typings/is-void';

/**
 * Gets the property descriptor of the specified object key.
 *
 * A property descriptor is one that is defined directly on the object or inherited from the
 * object's prototype chain.
 * A property descriptor is undefined if the property does not exist or its value is undefined.
 * @param obj The object that contains the property.
 * @param key The key of the property.
 * @returns The property descriptor or `undefined` if does not exist or its value is undefined.
 */
export function getPropertyDescriptor(obj: any, key: PropertyKey): PropertyDescriptor | undefined {
  if (typeof obj !== 'object') {
    return undefined;
  }

  let proto: {} = obj;
  let descriptor: PropertyDescriptor | undefined;

  do {
    descriptor = Object.getOwnPropertyDescriptor(proto, key);
  } while (descriptor === undefined && !isVoid((proto = Object.getPrototypeOf(proto))));

  return descriptor;
}
