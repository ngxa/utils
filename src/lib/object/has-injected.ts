/**
 * Check if a class in injected in an object.
 *
 * @param obj The object that contains the injection.
 * @param clazz The class to tes if is injected.
 * @returns `true` if he class is injected, `false` otherwise.
 */
export function hasInjected<T extends object>(obj: T, clazz: any): boolean {
  return Object.keys(obj).some((key: string) => (obj as any)[key] instanceof clazz);
}
