/**
 * Check if the object inherits from a class.
 *
 * @param obj The object to test.
 * @param clazz The class to test inheritance from.
 * @returns `true` if the object inherits from the class, `false` otherwise.
 */
export function objectInheritsFrom<O extends object>(obj: O, clazz: any): boolean {
  return Object.getPrototypeOf(obj) instanceof clazz;
}
