import { hasSetter } from './has-setter';

/**
 * Check if key is a settable property.
 *
 * @export
 * @param obj The object that contains the property or setter.
 * @param key The key of the property or setter.
 * @returns `true` if the key is a defined settable property, if not `false`.
 */
export function hasSettable(obj: any, key: PropertyKey): boolean {
  return typeof obj === 'object' && (obj.hasOwnProperty(key) || hasSetter(obj, key));
}
