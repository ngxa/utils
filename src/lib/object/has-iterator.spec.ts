import { testData } from '../data/test.data';
import { TestEntry } from '../typings/test-entry.type';
import { hasIterator } from './has-iterator';

describe('hasIterator(value)', () => {
  test('returns true value is Subscribable', () => {
    testData
      .match('Iterator')
      .forEach((entry: TestEntry) => expect(hasIterator(entry[1])).toBeTruthy());
  });

  test('returns false as type guard if value is not Subscribable', () => {
    testData
      .mismatch('Iterator')
      .forEach((entry: TestEntry) => expect(hasIterator(entry[1])).toBeFalsy());
  });
});
