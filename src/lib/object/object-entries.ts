import { Entry } from '../dictionary/types/entry.type';
import { TestEntry } from '../typings/test-entry.type';
import { ObjectLiteral } from './types/object-literal.interface';

/**
 * Returns an array of a given object's own enumerable property [key, value] pairs, in the same
 * order as that provided by a for...in loop
 *
 * @param obj The object whose enumerable own property [key, value] pairs are to be returned.
 * @returns An array of the given object's own enumerable property [key, value] pairs.
 */
export function objectEntries(obj: ObjectLiteral): Entry<string, any>[] {
  return Object.keys(obj).reduce((entries: TestEntry[], key: string) => {
    entries.push([key, obj[key]]);

    return entries;
  }, []);
}
