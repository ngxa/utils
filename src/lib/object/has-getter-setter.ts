import { hasGetter } from './has-getter';
import { hasSetter } from './has-setter';

/**
 * Check if key is a defined getter or setter.
 *
 * @param obj The object that contains the getter or setter.
 * @param key The key of the getter or setter.
 * @returns `true` if the key is a defined getter or setter, if not `false`.
 */
export function hasGetterSetter(obj: any, key: PropertyKey): boolean {
  return typeof obj === 'object' && (hasGetter(obj, key) || hasSetter(obj, key));
}
