import { TestClass } from '../data/test-class.data';

import { hasMember } from './has-member';

describe('hasMember(obj, key)', () => {
  let obj: TestClass;

  beforeEach(() => {
    obj = new TestClass();
  });

  test('returns false for non object', () => {
    expect(hasMember('', 'prop')).toEqual(false);
  });

  test('returns false for non existant properties', () => {
    expect(hasMember(obj, 'nonExistantProp')).toEqual(false);
  });

  test('returns false for undefined own properties', () => {
    expect(hasMember(obj, 'undefinedProp')).toEqual(false);
  });

  test('returns false for undefined properties in prototype chain', () => {
    expect(hasMember(obj, 'undefinedParentProp')).toEqual(false);
    expect(hasMember(obj, 'undefinedGrandProp')).toEqual(false);
  });

  test('returns true for own properties', () => {
    expect(hasMember(obj, 'prop')).toEqual(true);
  });

  test('returns true for properties in prototype chain', () => {
    expect(hasMember(obj, 'parentProp')).toEqual(true);
    expect(hasMember(obj, 'grandProp')).toEqual(true);
  });

  test('returns true for own getters/setters', () => {
    expect(hasMember(obj, 'getterSetterProp')).toEqual(true);
  });

  test('returns true for getters/setters in prototype chain', () => {
    expect(hasMember(obj, 'getterSetterParentProp')).toEqual(true);
    expect(hasMember(obj, 'getterSetterGrandProp')).toEqual(true);
  });

  test('returns true for own getters', () => {
    expect(hasMember(obj, 'getterProp')).toEqual(true);
  });

  test('returns true for getters in prototype chain', () => {
    expect(hasMember(obj, 'getterParentProp')).toEqual(true);
    expect(hasMember(obj, 'getterGrandProp')).toEqual(true);
  });

  test('returns true for own setters', () => {
    expect(hasMember(obj, 'setterProp')).toEqual(true);
  });

  test('returns true for setters in prototype chain', () => {
    expect(hasMember(obj, 'setterParentProp')).toEqual(true);
    expect(hasMember(obj, 'setterGrandProp')).toEqual(true);
  });

  test('returns true for own methods', () => {
    expect(hasMember(obj, 'method')).toEqual(true);
  });

  test('returns true for methods in prototype chain', () => {
    expect(hasMember(obj, 'parentMethod')).toEqual(true);
    expect(hasMember(obj, 'grandMethod')).toEqual(true);
  });
});
