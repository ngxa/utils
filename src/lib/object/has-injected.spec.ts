import { GrandTestClass } from '../data/grand-test-class.data';
import { OtherTestClass } from '../data/other-test-class.data';
import { ParentTestClass } from '../data/parent-test-class.data';
import { TestClass } from '../data/test-class.data';
import { hasInjected } from './has-injected';

class LocalTestClass {
  public publicInjected: TestClass = new TestClass();
  protected protectedInjected: ParentTestClass = new ParentTestClass();
  private privateInjected: GrandTestClass = new GrandTestClass();
}

describe('hasInjected(obj, clazz)', () => {
  let obj: LocalTestClass;

  beforeEach(() => {
    obj = new LocalTestClass();
  });

  test('returns true for public injected', () => {
    expect(hasInjected(obj, TestClass)).toBeTruthy();
  });

  test('returns true for protected injected', () => {
    expect(hasInjected(obj, ParentTestClass)).toBeTruthy();
  });

  test('returns true for private injected', () => {
    expect(hasInjected(obj, GrandTestClass)).toBeTruthy();
  });

  test('returns false for not injected', () => {
    expect(hasInjected(obj, OtherTestClass)).toBeFalsy();
  });
});
