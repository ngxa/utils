import { excludeObjectKeys } from './exclude-object-keys';

const fromObj: {} = { aAA: 1, aAB: 2, aC: 3 };

describe('excludeObjectKeys(obj, ...keys)', () => {
  test('returns empty object from empty object', () => {
    expect(excludeObjectKeys({}, 'aAA')).toEqual({});
  });

  test('returns the original object if no keys', () => {
    expect(excludeObjectKeys(fromObj)).toEqual(fromObj);
  });

  test('returns object without single key', () => {
    expect(excludeObjectKeys(fromObj, 'aAA')).toEqual({ aAB: 2, aC: 3 });
  });

  test('returns object without multiple keys', () => {
    expect(excludeObjectKeys(fromObj, 'aAA', 'aAB')).toEqual({ aC: 3 });
  });

  test('returns object avoiding non existant keys', () => {
    expect(excludeObjectKeys(fromObj, 'aAA', 'nonExistant')).toEqual({ aAB: 2, aC: 3 });
  });
});
