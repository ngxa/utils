import { ObjectKey } from './types/object-key.type';
import { ObjectLiteral } from './types/object-literal.interface';

/**
 * Get a new object without the excluded keys and its values.
 *
 * @param obj Original object.
 * @param keys Keys to exclude from the new object.
 * @returns The object without the excluded keys and its values.
 */
export function excludeObjectKeys<T extends ObjectLiteral>(obj: T, ...keys: ObjectKey[]): T {
  return Object.keys(obj).reduce((newObj: T, key: ObjectKey) => {
    if (keys.indexOf(key) === -1) {
      newObj[key] = obj[key];
    }

    return newObj;
  }, Object.create(null));
}
