module.exports = {
  preset: 'jest-preset-angular',
  roots: ['src'],
  setupTestFrameworkScriptFile: '<rootDir>/src/setup-jest.ts',
  collectCoverageFrom: ['src/lib/**/*.ts', '!**/*.data.ts'],
  coverageReporters: ['json', 'lcov', 'html', 'text-summary'],
  reporters: [
    'default',
    [
      './node_modules/jest-html-reporter',
      {
        pageTitle: '@ngxa/utils Test Report',
        outputPath: './coverage/test-report.html'
      }
    ]
  ]
};
