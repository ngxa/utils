## [0.17.1](https://gitlab.com/ngxa/utils/compare/v0.17.0...v0.17.1) (2019-01-01)


### Bug Fixes

* **testbed:** changes in testbed config ([ef37f5d](https://gitlab.com/ngxa/utils/commit/ef37f5d))

# [0.17.0](https://gitlab.com/ngxa/utils/compare/v0.16.0...v0.17.0) (2018-12-26)


### Bug Fixes

* **filter-object-keys:** fix typing ([d360fcd](https://gitlab.com/ngxa/utils/commit/d360fcd))
* **style:** fix styles ([67b3012](https://gitlab.com/ngxa/utils/commit/67b3012))


### Features

* **is-not-empty-array:** function to check that is a non empty Array ([226f616](https://gitlab.com/ngxa/utils/commit/226f616))

# [0.16.0](https://gitlab.com/ngxa/utils/compare/v0.15.1...v0.16.0) (2018-12-10)


### Bug Fixes

* **defaults:** fix types ([499ca4a](https://gitlab.com/ngxa/utils/commit/499ca4a))


### Features

* **input-change:** add function to check that an Input has changed ([5bf0579](https://gitlab.com/ngxa/utils/commit/5bf0579))
* **object:** add cleanObject function to delete void and empty class values ([25f14b4](https://gitlab.com/ngxa/utils/commit/25f14b4))

## [0.15.1](https://gitlab.com/ngxa/utils/compare/v0.15.0...v0.15.1) (2018-12-05)


### Bug Fixes

* **defaults:** fix type ([c911d73](https://gitlab.com/ngxa/utils/commit/c911d73))

# [0.15.0](https://gitlab.com/ngxa/utils/compare/v0.14.0...v0.15.0) (2018-11-30)


### Bug Fixes

* **object:** allow object literal without template ([46f562e](https://gitlab.com/ngxa/utils/commit/46f562e))
* **typings:** remove angular-l10n dependency ([ee3e90a](https://gitlab.com/ngxa/utils/commit/ee3e90a))


### Features

* **object:** new function to exclude keys from an object ([23e89be](https://gitlab.com/ngxa/utils/commit/23e89be))

# [0.14.0](https://gitlab.com/ngxa/utils/compare/v0.13.0...v0.14.0) (2018-10-20)


### Features

* **object:** add hasInjected ([389976a](https://gitlab.com/ngxa/utils/commit/389976a))

# [0.13.0](https://gitlab.com/ngxa/utils/compare/v0.12.0...v0.13.0) (2018-10-19)


### Bug Fixes

* **defaults:** change defaults to check null and undefined ([cc3ae05](https://gitlab.com/ngxa/utils/commit/cc3ae05))


### Features

* **api:** add testEntry and objectInheritsFrom ([8819c69](https://gitlab.com/ngxa/utils/commit/8819c69))
* **object:** add objectInheritsFrom ([8b24911](https://gitlab.com/ngxa/utils/commit/8b24911))

# [0.12.0](https://gitlab.com/ngxa/utils/compare/v0.11.1...v0.12.0) (2018-10-15)


### Bug Fixes

* **l10n:** fix documentation and exports ([65aef29](https://gitlab.com/ngxa/utils/commit/65aef29))


### Features

* **l10n:** add l10n as dep and configs to use it ([c6fed32](https://gitlab.com/ngxa/utils/commit/c6fed32))
* **typings:** add typings from testing ([c8cc791](https://gitlab.com/ngxa/utils/commit/c8cc791))

## [0.11.1](https://gitlab.com/ngxa/utils/compare/v0.11.0...v0.11.1) (2018-08-30)


### Bug Fixes

* **defaults:** add undefined to arg ([6dfe780](https://gitlab.com/ngxa/utils/commit/6dfe780))

# [0.11.0](https://gitlab.com/ngxa/utils/compare/v0.10.0...v0.11.0) (2018-08-30)


### Bug Fixes

* **defaults:** fix code ([a7d5356](https://gitlab.com/ngxa/utils/commit/a7d5356))


### Features

* **defaults:** add defaults function ([dbe926a](https://gitlab.com/ngxa/utils/commit/dbe926a))

# [0.10.0](https://gitlab.com/ngxa/utils/compare/v0.9.5...v0.10.0) (2018-08-29)


### Bug Fixes

* **data:** add more methods to test ([fd09881](https://gitlab.com/ngxa/utils/commit/fd09881))


### Features

* **string:** add typeToString() ([680926f](https://gitlab.com/ngxa/utils/commit/680926f))

## [0.9.5](https://gitlab.com/ngxa/utils/compare/v0.9.4...v0.9.5) (2018-08-29)


### Bug Fixes

* **data:** add more methods to class tets ([94756f1](https://gitlab.com/ngxa/utils/commit/94756f1))

## [0.9.4](https://gitlab.com/ngxa/utils/compare/v0.9.3...v0.9.4) (2018-08-25)


### Bug Fixes

* **data:** add more methods ([eaa0132](https://gitlab.com/ngxa/utils/commit/eaa0132))

## [0.9.3](https://gitlab.com/ngxa/utils/compare/v0.9.2...v0.9.3) (2018-08-25)


### Bug Fixes

* **data:** update methods ([616e75c](https://gitlab.com/ngxa/utils/commit/616e75c))

## [0.9.2](https://gitlab.com/ngxa/utils/compare/v0.9.1...v0.9.2) (2018-08-25)


### Bug Fixes

* **data:** add more methods to test ([440bb25](https://gitlab.com/ngxa/utils/commit/440bb25))

## [0.9.1](https://gitlab.com/ngxa/utils/compare/v0.9.0...v0.9.1) (2018-08-25)


### Bug Fixes

* **data:** add more method calls ([72dde5b](https://gitlab.com/ngxa/utils/commit/72dde5b))

# [0.9.0](https://gitlab.com/ngxa/utils/compare/v0.8.0...v0.9.0) (2018-08-24)


### Features

* **object:** add hasGettable() and hasSettable() ([4d41b2c](https://gitlab.com/ngxa/utils/commit/4d41b2c))

# [0.8.0](https://gitlab.com/ngxa/utils/compare/v0.7.1...v0.8.0) (2018-08-23)


### Features

* **data:** add OtherTestClass ([ec72908](https://gitlab.com/ngxa/utils/commit/ec72908))

## [0.7.1](https://gitlab.com/ngxa/utils/compare/v0.7.0...v0.7.1) (2018-08-23)


### Bug Fixes

* **ci:** fix ci ([1784998](https://gitlab.com/ngxa/utils/commit/1784998))
* **ci:** fix ci coverage ([a49da1a](https://gitlab.com/ngxa/utils/commit/a49da1a))

# [0.7.0](https://gitlab.com/ngxa/utils/compare/v0.6.0...v0.7.0) (2018-08-19)


### Bug Fixes

* **api:** api changes ([303471c](https://gitlab.com/ngxa/utils/commit/303471c))
* **api:** fix api ([7c5d07d](https://gitlab.com/ngxa/utils/commit/7c5d07d))
* **api:** refactor api ([d249298](https://gitlab.com/ngxa/utils/commit/d249298))
* **api:** update api ([1326427](https://gitlab.com/ngxa/utils/commit/1326427))
* **data:** fix testData ([15bd65b](https://gitlab.com/ngxa/utils/commit/15bd65b))
* **dictionary:** allow undefined constructor ([f10ee62](https://gitlab.com/ngxa/utils/commit/f10ee62))
* **dictionary:** do not mutate on reduceReverse() and do not use internal methods ([a718341](https://gitlab.com/ngxa/utils/commit/a718341))
* **dictionary:** fix reduce first element shift ([dfe6eb6](https://gitlab.com/ngxa/utils/commit/dfe6eb6))
* **filter:** first working version of KeyFilter ([e59de0b](https://gitlab.com/ngxa/utils/commit/e59de0b))
* **setup:** fix setup ([ae32fb8](https://gitlab.com/ngxa/utils/commit/ae32fb8))


### Features

* **array:** add arrayIncludes() ([7fb5817](https://gitlab.com/ngxa/utils/commit/7fb5817))
* **data:** add testData (wip) ([28c43b3](https://gitlab.com/ngxa/utils/commit/28c43b3))
* **data:** add testData const ([85b43d6](https://gitlab.com/ngxa/utils/commit/85b43d6))
* **dictionary:** add DictionaryKey ([8344a49](https://gitlab.com/ngxa/utils/commit/8344a49))
* **dictionary:** add every() and some() methods ([a88fbfb](https://gitlab.com/ngxa/utils/commit/a88fbfb))
* **dictionary:** add map() and modify pop() and shift() ([a680d63](https://gitlab.com/ngxa/utils/commit/a680d63))
* **dictionary:** add match() and mismatch(), and fix reduce() and reduceRight() ([331afa3](https://gitlab.com/ngxa/utils/commit/331afa3))
* **dictionary:** add pop(), shift(), sort() and reverse() ([3e2d49a](https://gitlab.com/ngxa/utils/commit/3e2d49a))
* **dictionary:** add reduce() and reduceRight() ([02b2e47](https://gitlab.com/ngxa/utils/commit/02b2e47))
* **dictionary:** change keys() and valuex() to iterator and refactor most of the code ([cf17f54](https://gitlab.com/ngxa/utils/commit/cf17f54))
* **dictionary:** rewrite of iterator methods ([fcfde83](https://gitlab.com/ngxa/utils/commit/fcfde83))
* **dictionary:** wip: add new functionality and tests ([b735cf9](https://gitlab.com/ngxa/utils/commit/b735cf9))
* **dictionary:** wip: dictionary data type ([9ec351b](https://gitlab.com/ngxa/utils/commit/9ec351b))
* **dictionary:** wip: Dictionary data type ([40522a1](https://gitlab.com/ngxa/utils/commit/40522a1))
* **dictionary:** wip: tests and small fixes ([2673d88](https://gitlab.com/ngxa/utils/commit/2673d88))
* **filter:** add keys() and values() to FilterKeys and some refactoring ([226f198](https://gitlab.com/ngxa/utils/commit/226f198))
* **filter:** add the skeleton for the new filter class ([f55c0ba](https://gitlab.com/ngxa/utils/commit/f55c0ba)), closes [#1](https://gitlab.com/ngxa/utils/issues/1)
* **filter:** allow filter by function ([b542b3e](https://gitlab.com/ngxa/utils/commit/b542b3e))
* **filter:** wip: filterKey ([8ec892f](https://gitlab.com/ngxa/utils/commit/8ec892f))
* **key-value:** add KeyType ([93fd95a](https://gitlab.com/ngxa/utils/commit/93fd95a))
* **key-value:** add toMap ([c096300](https://gitlab.com/ngxa/utils/commit/c096300))
* **key-value:** add toObject ([2eb2c63](https://gitlab.com/ngxa/utils/commit/2eb2c63))
* **key-value:** add toTuple ([62e0d37](https://gitlab.com/ngxa/utils/commit/62e0d37))
* **lib:** add sameValueZero() ([c9fcd83](https://gitlab.com/ngxa/utils/commit/c9fcd83))
* **object:** add objectEntries() ([8f78e24](https://gitlab.com/ngxa/utils/commit/8f78e24))
* **type-guard:** add isIterator ([7485b77](https://gitlab.com/ngxa/utils/commit/7485b77))
* **type-guard:** wip: isEnumerable ([d0ce1c1](https://gitlab.com/ngxa/utils/commit/d0ce1c1))


### Performance Improvements

* **dictionary:** better match() and mismatch() implementation ([5e33919](https://gitlab.com/ngxa/utils/commit/5e33919))
* **dictionary:** safer and less complex every() and some() methods ([3589108](https://gitlab.com/ngxa/utils/commit/3589108))

# [0.6.0](https://gitlab.com/ngxa/utils/compare/v0.5.0...v0.6.0) (2018-08-04)


### Bug Fixes

* **api:** update api ([cbb5d5a](https://gitlab.com/ngxa/utils/commit/cbb5d5a))


### Features

* **data:** add more options and refactor test class ([7f2865a](https://gitlab.com/ngxa/utils/commit/7f2865a))
* **typings:** add arePk ([ad4459f](https://gitlab.com/ngxa/utils/commit/ad4459f))

# [0.5.0](https://gitlab.com/ngxa/utils/compare/v0.4.0...v0.5.0) (2018-08-03)


### Bug Fixes

* **api:** update public api ([912d073](https://gitlab.com/ngxa/utils/commit/912d073))


### Features

* **object:** add getPropertyDescriptor ([b7c03e4](https://gitlab.com/ngxa/utils/commit/b7c03e4))
* **object:** add hasGetter ([04907a1](https://gitlab.com/ngxa/utils/commit/04907a1))
* **object:** add hasGetterSetter ([c91e9a2](https://gitlab.com/ngxa/utils/commit/c91e9a2))
* **object:** add hasMember ([cc299f5](https://gitlab.com/ngxa/utils/commit/cc299f5))
* **object:** add hasMethod ([a226c22](https://gitlab.com/ngxa/utils/commit/a226c22))
* **object:** add hasProperty ([afc7815](https://gitlab.com/ngxa/utils/commit/afc7815))
* **object:** add isObjectMember ([a9956f1](https://gitlab.com/ngxa/utils/commit/a9956f1))
* **object:** hasSetter ([67f6409](https://gitlab.com/ngxa/utils/commit/67f6409))
* **utils:** add class test data ([baeaf7d](https://gitlab.com/ngxa/utils/commit/baeaf7d))

# [0.4.0](https://gitlab.com/ngxa/utils/compare/v0.3.0...v0.4.0) (2018-08-02)


### Features

* **api:** rewrite api ([1b5668e](https://gitlab.com/ngxa/utils/commit/1b5668e))
* **are-all:** remove functionality ([1a58331](https://gitlab.com/ngxa/utils/commit/1a58331))
* **combine:** rewrite and refactor ([b2ab59d](https://gitlab.com/ngxa/utils/commit/b2ab59d))
* **filter:** rewrite and refactor ([c53459c](https://gitlab.com/ngxa/utils/commit/c53459c))
* **test-values:** rewrite and refactor ([9602b8f](https://gitlab.com/ngxa/utils/commit/9602b8f))
* **type-guards:** remove is-regexp ([02931b9](https://gitlab.com/ngxa/utils/commit/02931b9))
* **type-guards:** rewrite and refactor is-object ([4bb4fa2](https://gitlab.com/ngxa/utils/commit/4bb4fa2))
* **type-guards:** rewrite and refactor is-subscribable ([71673a0](https://gitlab.com/ngxa/utils/commit/71673a0))
* **type-guards:** rewrite and refactor is-void ([1376e40](https://gitlab.com/ngxa/utils/commit/1376e40))
* **typings:** rewrite and refactor pk ([df453c0](https://gitlab.com/ngxa/utils/commit/df453c0))
* **unique:** rewrite and refactor ([4e6630c](https://gitlab.com/ngxa/utils/commit/4e6630c))
* **utils:** add getPropertyDescriptor ([2eaa614](https://gitlab.com/ngxa/utils/commit/2eaa614))
* **values:** rewrite and refactor ([9db3647](https://gitlab.com/ngxa/utils/commit/9db3647))

# [0.3.0](https://gitlab.com/ngxa/utils/compare/v0.2.0...v0.3.0) (2018-07-14)


### Bug Fixes

* **data:** add boolean, string, number and rexexp, as well fixed keys ([61c4a97](https://gitlab.com/ngxa/utils/commit/61c4a97))
* **type-guards:** change isRegExp implementation ([5fcbdc8](https://gitlab.com/ngxa/utils/commit/5fcbdc8))
* **type-guards:** change order ([c9e53f9](https://gitlab.com/ngxa/utils/commit/c9e53f9))
* **type-guards:** exclude Array and null from object definition ([1d97963](https://gitlab.com/ngxa/utils/commit/1d97963))
* **unique:** use Set from ES6 ([2119ba6](https://gitlab.com/ngxa/utils/commit/2119ba6))


### Features

* **filter-object:** add support for RegExp as terms ([ed5180d](https://gitlab.com/ngxa/utils/commit/ed5180d))
* **is-regexp:** implement RexExp type guard ([7b00858](https://gitlab.com/ngxa/utils/commit/7b00858))
* **typings:** implement Optional type ([07e662d](https://gitlab.com/ngxa/utils/commit/07e662d))

# [0.2.0](https://gitlab.com/ngxa/utils/compare/v0.1.0...v0.2.0) (2018-07-09)


### Features

* **data:** add componentMeta, a basic [@component](https://gitlab.com/component) metadata ([8247e5e](https://gitlab.com/ngxa/utils/commit/8247e5e))

# [0.1.0](https://gitlab.com/ngxa/utils/compare/v0.0.1...v0.1.0) (2018-07-09)


### Bug Fixes

* **type-guards:** fix unneeded type assertion ([cb30f03](https://gitlab.com/ngxa/utils/commit/cb30f03))


### Features

* **type-guards:** add isSubscribable function ([bf6cc7c](https://gitlab.com/ngxa/utils/commit/bf6cc7c))
