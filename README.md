# @ngxa/utils

Angular utils library to share common code.

[![npm](https://img.shields.io/npm/v/@ngxa/utils.svg)](https://www.npmjs.com/package/@ngxa/utils)
[![NpmLicense](https://img.shields.io/npm/l/@ngxa/utils.svg)](https://gitlab.com/ngxa/utils/blob/master/LICENSE.md)
[![coverage report](https://gitlab.com/ngxa/utils/badges/master/coverage.svg)](https://gitlab.com/ngxa/utils/commits/master)
[![sonarqube](https://sonarcloud.io/api/project_badges/measure?project=ngxa%3Autils&metric=alert_status)](https://sonarcloud.io/dashboard?id=ngxa%3Autils)
[![documentation](https://img.shields.io/badge/web-docs-orange.svg)](http://ngxa.gitlab.io/utils)
[![coverage](https://img.shields.io/badge/web-coverage-orange.svg)](http://ngxa.gitlab.io/utils/coverage/)
[![coverage](https://img.shields.io/badge/web-tests-orange.svg)](http://ngxa.gitlab.io/utils/tests/)

## Installation

You can install @ngxa/utils, and all its dependencies, using npm.

```shell
npm install --save @ngxa/utils
```
